﻿Imports System.IO
Imports System.Text
Imports System.Xml
Imports System.Security.Principal

Public Class MHO
    ' after updating resources, update currentVersion
    Dim currentVersion As String = "2.0.11.632"

    Dim patchVersion As String = My.Resources.patchversion

    Dim MHOFolderPath As String = Nothing
    Dim utf8WithoutBom As New UTF8Encoding(False)
    Dim patchName As String = GetHash(currentVersion)
    Dim patchReady As Boolean = False
    Dim versionCompatible As Boolean = False
    Dim readyToclose As Boolean = False
    Dim unloadingInProgress As Boolean = False

    Dim gamefolderokay As Boolean = False
    Dim autostartpatch As Integer = 0
    Dim minimizetotray As Integer = 0
    Dim forceClose As Boolean = False

    Public localAppDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\MHOEnglishPatch\"

    Dim selectFolderRetryMax As Integer = 3
    Dim selectFolderRetyCount As Integer = 0

    Dim configGamePath As String = ""

    Dim PAYPAL_LINK As String = "https://goo.gl/WiQovF"
    Dim DISCORD_LINK As String = "https://goo.gl/Efcygv"
    Dim PATREON_LINK As String = "https://goo.gl/6NTsT6"
    Dim BITCOIN_LINK As String = "https://goo.gl/xq1GMp"
    Dim FACEBOOK_LINK As String = "https://goo.gl/1D1bzk"
    Dim REDDIT_LINK As String = "https://goo.gl/JtEyVG"

    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.Closing
        If unloadingInProgress Then
            updateLoggerContent("Unloading patch in progress, please wait")
            e.Cancel = True
        ElseIf forceClose Then
            e.Cancel = False
        Else
            If Not isGameProcessesRunning() Then
                If versionCompatible And Not readyToclose And patchReady Then
                    updateLoggerContent("Unloading patch, this will automatically close itself.")
                    disableStartPatchBtn()
                    unloadingInProgress = True
                    e.Cancel = True

                    ' will be show as balloon tips if in tray
                    logMessageWhenInTray("Unloading patch, this will automatically close itself.", 3)

                    TimeOut(AddressOf unloadFiles, 4000)
                End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub Form1_resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        ' minimize to tray if option enabled
        If minimizetotray Then
            hideFormToTaskbar()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' show versions
        versionLabel.Text = "Compatible with MHO v" + currentVersion
        gameversiontxt.Text = "Game: " + currentVersion
        gameversiontxtcontext.Text = gameversiontxt.Text
        patchversiontxt.Text = "English patch: " + patchVersion
        patchversiontxtcontext.Text = patchversiontxt.Text
        Me.Text = "Monster Hunter Online - English Patch v" + patchVersion
        NotifyIcon1.Text = Me.Text

        If Not isGameProcessesRunning(False) Then

            Dim identity = WindowsIdentity.GetCurrent()
            Dim principal = New WindowsPrincipal(identity)
            Dim isElevated As Boolean = principal.IsInRole(WindowsBuiltInRole.Administrator)

            If Not isElevated Then
                MsgBox("Unable to start patch launcher." + vbCrLf + "Please run the program as an Administrator.", MsgBoxStyle.Critical, "Critical Error - Meow meow!")
                Me.Close()
            End If

            ' read config of strip menu
            readStripMenuConfigs()

            ' start client checks timer
            MHOGameClientTicker.Start()

            ' prepare files after 2 seconds
            TimeOut(AddressOf prepareFiles, 2000)
        Else
            MsgBox("Monster Hunter Online is currently running, please close it.", MsgBoxStyle.Critical)
            forceClose = True
            Me.Close()
        End If
    End Sub

    Private Sub readStripMenuConfigs()
        ' read config for autopatch option
        Dim autostartpatchconfig = getConfig("autostartpatch")
        autostartpatch = IIf(autostartpatchconfig IsNot Nothing And autostartpatchconfig IsNot "" And autostartpatchconfig = "1", 1, 0)
        If autostartpatch = 1 Then
            AutorunPatchToolStripMenuItem.Checked = True
        Else
            AutorunPatchToolStripMenuItem.Checked = False
        End If

        ' read config for minimize to tray option
        Dim minimizetotrayconfig = getConfig("minimizetotray")
        minimizetotray = IIf(minimizetotrayconfig IsNot Nothing And minimizetotrayconfig IsNot "" And minimizetotrayconfig = "1", 1, 0)
        If minimizetotray = 1 Then
            MinimizeToTrayToolStripMenuItem.Checked = True
        Else
            MinimizeToTrayToolStripMenuItem.Checked = False
        End If
    End Sub


    Private Sub unloadFiles()
        Try
            ' stop timer for checking MHOClient status
            MHOGameClientTicker.Stop()

            Dim patchFile As String = getPatchFilePath()
            Dim IIPSFileList As String = getIIPSFileListPath()

            ' restore IIPSFileList on close
            If My.Computer.FileSystem.FileExists(IIPSFileList) Then
                My.Computer.FileSystem.DeleteFile(IIPSFileList)
                My.Computer.FileSystem.WriteAllText(IIPSFileList, My.Resources.IIPSFileList, True, utf8WithoutBom)
                'updateLoggerContent("IIPSFileList restored: " + IIPSFileList)
            End If

            ' remove patch rom on close
            If My.Computer.FileSystem.FileExists(patchFile) Then
                ' unhide patch first
                unHidePatchRom()

                My.Computer.FileSystem.DeleteFile(patchFile)
                'updateLoggerContent("Patch rom deleted: " + patchFile)
            End If

            readyToclose = True
            unloadingInProgress = False
            Me.Close()
        Catch ex As Exception
            If ex.Message.ToString.Contains("denied") Then
                updateLoggerContent("Unloading files failed")
                updateLoggerContent("Try to run as Administrator")
                'updateLoggerContent("Error: " + ex.Message.ToString)
            End If
        End Try
    End Sub

    Private Sub prepareFiles()
        ' make logbox empty
        logbox.Text = Nothing

        Try
            ' before everything check if the game is running
            ' if not check if ipshostapp.exe is also running
            If Not isGameProcessesRunning() Then
                ' check config if the path is available
                Dim gamepathfromconfig As String = getConfig("gamepath")
                If gamepathfromconfig IsNot Nothing And gamepathfromconfig IsNot "" Then
                    MHOFolderPath = gamepathfromconfig
                    updateLoggerContent("We got MHO folder from config")
                    updateLoggerContent(MHOFolderPath)
                Else
                    ' read the mho folder path from regedit
                    MHOFolderPath = readRegEditForPath()
                    updateLoggerContent("We detected the MHO folder")
                End If

                ' set game folder strip
                setGameFolderStripMenu()

                ' verify mho folder
                verifyMHOFolder()

                ' check if the current version of the game is supported
                ' only when the game folder is found
                If gamefolderokay Then
                    updateLoggerContent("Checking versions...")

                    ' check if mmog xml exist
                    If My.Computer.FileSystem.FileExists(getMMOGDataPath()) Then
                        Dim gamemmogversion As String = ""
                        Try
                            Dim m_xmld As XmlDocument
                            Dim m_nodelist As XmlNodeList
                            Dim m_node As XmlNode
                            'Create the XML Document
                            m_xmld = New XmlDocument()
                            'Load the Xml file
                            m_xmld.Load(getMMOGDataPath())
                            'Get the list of name nodes 
                            m_nodelist = m_xmld.SelectNodes("/VersionUpdateData/ClientConf/VersionData")
                            'Loop through the nodes
                            For Each m_node In m_nodelist
                                If m_node.ChildNodes.Item(0).Name = "Version" Then
                                    gamemmogversion = m_node.ChildNodes.Item(0).InnerText
                                End If
                            Next
                        Catch errorVariable As Exception
                            'Error trapping
                            updateLoggerContent(errorVariable.ToString())
                        End Try

                        ' check if we have the same
                        If currentVersion = gamemmogversion Then
                            versionCompatible = True
                            updateLoggerContent("Version compatible")
                            'updateLoggerContent("This patch is compatible with: " + currentVersion)
                            'updateLoggerContent("Your current game version is: " + gamemmogversion)
                            updateLoggerContent("Click start to apply the patch")

                            ' from here make sure to reset the iipsfilelist
                            Dim IIPSFileList As String = getIIPSFileListPath()
                            If My.Computer.FileSystem.FileExists(IIPSFileList) Then
                                My.Computer.FileSystem.DeleteFile(IIPSFileList)
                                My.Computer.FileSystem.WriteAllText(IIPSFileList, My.Resources.IIPSFileList, True, utf8WithoutBom)
                            End If

                            ' remove patch if still existed
                            Dim patchFile As String = getPatchFilePath()
                            If My.Computer.FileSystem.FileExists(patchFile) Then
                                ' unhide patch first
                                unHidePatchRom()

                                My.Computer.FileSystem.DeleteFile(patchFile)
                            End If

                            ' auto start
                            If autostartpatch Then
                                startPatchProcess()
                            Else
                                ' will be show as balloon tips if in tray
                                logMessageWhenInTray("Version compatible, start to apply the patch.", 3)
                            End If
                        Else
                            updateLoggerContent("Version incompatible")
                            updateLoggerContent("This patch is only compatible with: " + currentVersion)
                            updateLoggerContent("Your current game version is: " + gamemmogversion)
                            updateLoggerContent("Checkout the Discord #english-patch channel for updates")
                            updateLoggerContent("https://discord.gg/0gQrOfuthFJDbIsh")
                            updateLoggerContent("If you see no updates, please refrain from asking questions")
                            updateLoggerContent("have some patience, we will release it as soon as we can")
                            disableStartPatchBtn()
                            StartPatch.Text = "Incompatible".ToUpper
                            StartPatchToolStrip.Text = "Incompatible"

                            ' will be show as balloon tips if in tray
                            logMessageWhenInTray("Version incompatible, open for more info.", 3)
                        End If
                    Else
                        updateLoggerContent("Required file for version checking is missing")
                        updateLoggerContent("Reinstall your game to run this patch")
                        MsgBox("Required file for version checking is missing." + vbCrLf + "You may need to reinstall your game to run this patch.", MsgBoxStyle.Critical, "Critical Error")
                        disableStartPatchBtn()

                        ' will be show as balloon tips if in tray
                        logMessageWhenInTray("Required file went missing, open for more info.", 3)
                    End If
                End If
            End If
        Catch ex As Exception
            updateLoggerContent("Error: " + ex.Message)
        End Try
    End Sub

    Private Sub verifyMHOFolder(Optional ByVal fromConfig As Boolean = False)
        updateLoggerContent("Verifying MHO folder...")
        Dim MHoClientExePath = MHOFolderPath + "Bin\Client\Bin32\MHOClient.exe"
        If MHOFolderPath IsNot Nothing And My.Computer.FileSystem.FileExists(MHoClientExePath) Then
            updateLoggerContent("MHO folder is valid")
            gamefolderok.Enabled = True

            ' flag gamefolderokay variable
            gamefolderokay = True

            ' save game folder to config
            configGamePath = MHOFolderPath
            writeConfig()

            ' enable patch button
            enableStartPatchBtn()
        Else
            ' check if it can be found inside config
            Dim gamepathfromconfig = getConfig("gamepath")
            If gamepathfromconfig IsNot Nothing And gamepathfromconfig IsNot "" Then
                MHOFolderPath = gamepathfromconfig

                ' if from a looping sequence and still not valid
                ' mark the config folder as empty now and rewrite config
                If fromConfig Then
                    configGamePath = ""
                    writeConfig()
                    updateLoggerContent("MHO folder found in config but invalid")
                    verifyMHOFolder(True)
                Else
                    updateLoggerContent("MHO folder found in config")
                    updateLoggerContent(MHOFolderPath)
                    verifyMHOFolder(True)
                End If
            Else
                updateLoggerContent("MHO folder is invalid")
                gamefolderok.Enabled = False
                selectMHOFolderManually()
                verifyMHOFolder()
            End If
        End If
    End Sub

    Private Sub selectMHOFolderManually(Optional ByVal withPrompt As Boolean = True, Optional ByVal persist As Boolean = True)
        selectGameFolderPath.Description = "Please find where you installed Monster Hunter Online"

        If withPrompt Then
            MsgBox("Unable to locate MHO Installation Folder. " + vbCrLf + "Find where you installed Monster Hunter Online, Thank you!", MsgBoxStyle.Information)
        End If

        Dim result As DialogResult = selectGameFolderPath.ShowDialog()

        If (result = DialogResult.OK) Then
            Dim folderName = selectGameFolderPath.SelectedPath
            folderName = IIf(folderName.EndsWith("\"), folderName, folderName + "\")
            updateLoggerContent("MHO manually set from")
            updateLoggerContent(folderName)
            MHOFolderPath = folderName
        Else
            ' check if persist param is enabled
            ' this will keep asking for the folder if not valid upto 3times
            If persist Then
                selectFolderRetyCount += 1

                If selectFolderRetyCount >= selectFolderRetryMax Then
                    MsgBox("Ok I'll try asking you again next time, Thanks!", MsgBoxStyle.Information)
                    Me.Close()
                Else
                    MsgBox("Sorry but we need that Folder. Please try again.", MsgBoxStyle.Information)
                    selectMHOFolderManually(False)
                End If

            End If
        End If
    End Sub

    Private Function readRegEditForPath() As String
        Dim keyNames() As String = {
            "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Monster Hunter Online",
            "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\怪物猎人Online"
        }

        Dim stringVals() As String = {
            "InstallSource",
            "InstallSource"
        }

        ' Iterate through the list by using nested loops.
        Dim found As String = Nothing
        For index As Integer = 0 To keyNames.Length - 1
            Dim keyName As String = keyNames(index)
            Dim stringVal As String = stringVals(index)
            Dim readValue = My.Computer.Registry.GetValue(keyName, stringVal, Nothing)

            ' register the mho path to be use later
            If readValue IsNot Nothing Then
                readValue = IIf(readValue.EndsWith("\"), readValue, readValue + "\")
                found = readValue
                updateLoggerContent("We got MHO folder from system")
                updateLoggerContent(readValue)
                Exit For
            End If
        Next

        Return found
    End Function


    Private Sub startPatchProcess()
        disableStartPatchBtn()
        StartPatch.Text = "Preparing patch".ToUpper

        Dim p() As Process = Process.GetProcessesByName("MHOClient")
        If p.Length > 0 Then
            MsgBox("Monster Hunter Online is currently running." + vbCrLf + "Please close the game first.", MsgBoxStyle.Critical, "Critical Error")
        ElseIf patchReady Then
            updateLoggerContent("Patch ready!")
            updateLoggerContent("Start your Monster Hunter Online and keep this window running")

            ' will be show as balloon tips if in tray
            logMessageWhenInTray("Patch ready, start Monster Hunter Online", 2)
        Else
            updateLoggerContent("Preparing patch, please wait...")
            TimeOut(AddressOf applyPatch, 2000)
        End If

    End Sub

    Private Sub applyPatch()
        Dim patchFile As String = getPatchFilePath()
        Dim IIPSFileList As String = getIIPSFileListPath()

        Try
            ' remove existing IIPSFileList
            If My.Computer.FileSystem.FileExists(IIPSFileList) Then
                My.Computer.FileSystem.DeleteFile(IIPSFileList)
            End If

            ' apply default IIPSFileList
            My.Computer.FileSystem.WriteAllText(IIPSFileList, My.Resources.IIPSFileList, True, utf8WithoutBom)
            Dim objWriter As New System.IO.StreamWriter(IIPSFileList, True, utf8WithoutBom)
            objWriter.WriteLine("[subversion]")
            objWriter.WriteLine("version=" + currentVersion)
            objWriter.WriteLine("patch=" + getPatchName())
            objWriter.Close()

            ' remove existing patch rom if any
            If My.Computer.FileSystem.FileExists(patchFile) Then
                My.Computer.FileSystem.DeleteFile(patchFile)
                'updateLoggerContent("Patch rom deleted: " + patchFile)
            End If

            ' write patch file
            IO.File.WriteAllBytes(patchFile, My.Resources.patch)

            ' hide patch rom
            hidePatchRom()

            updateLoggerContent("Patch ready!")
            updateLoggerContent("Start your Monster Hunter Online")
            updateLoggerContent("and keep this window open or minimize to tray")
            disableStartPatchBtn()
            StartPatch.Text = "Patch ready".ToUpper
            StartPatchToolStrip.Text = "Patch ready"
            patchhookok.Enabled = True
            patchReady = True

            logMessageWhenInTray("Patch ready, start Monster Hunter Online")
        Catch ex As Exception
            If ex.Message.ToString.Contains("denied") Then
                updateLoggerContent("Patch failed")
                updateLoggerContent("Try to run as Administrator")

                ' will be show as balloon tips if in tray
                logMessageWhenInTray("Patch failed, open for more info", 2)
                'updateLoggerContent("Error: " + ex.Message.ToString)
            End If
        End Try

        'Dim p() As Process = Process.GetProcessesByName("Client")
        'If p.Length > 0 Then
        'Else
        'updateLoggerContent("Launching Monster Hunter Online client...")
        'Process.Start(MHOFolderPath + "TCLS\Client.exe")
        'End If
    End Sub

    Private Function getConfig(ByVal configkey As String) As String
        Dim configvalue As String = Nothing
        Try
            ' read existing config ifany
            Dim configPath = getConfigFromLocalDataPath()
            If My.Computer.FileSystem.FileExists(configPath) Then
                Dim sr As New StreamReader(configPath)
                Dim line As String
                Dim searchStr As String = configkey
                Do While sr.Peek >= 0
                    line = sr.ReadLine()
                    If line.Contains(searchStr) Then
                        Dim versionsArr() As String = Split(line, "=")
                        configvalue = versionsArr(1)
                    End If
                Loop

                sr.Close()
                sr.Dispose()
            End If

            Return configvalue
        Catch ex As Exception
            If ex.Message.ToString.Contains("denied") Then
                updateLoggerContent("Unable to read config file")
                updateLoggerContent("Try to run as Administrator")
                'updateLoggerContent("Error: " + ex.Message.ToString)
            End If
            Return Nothing
        End Try
    End Function

    Private Sub writeConfig()
        Try
            ' remove existing config ifany
            Dim configPath = getConfigFromLocalDataPath()
            If My.Computer.FileSystem.FileExists(configPath) Then
                My.Computer.FileSystem.DeleteFile(configPath)
            End If

            Dim objWriter As New System.IO.StreamWriter(configPath, True, utf8WithoutBom)
            objWriter.WriteLine("gameversion=" + currentVersion)
            objWriter.WriteLine("patchversion=" + patchVersion)
            objWriter.WriteLine("gamepath=" + configGamePath)
            objWriter.WriteLine("autostartpatch=" + autostartpatch.ToString)
            objWriter.WriteLine("minimizetotray=" + minimizetotray.ToString)
            objWriter.Close()
        Catch ex As Exception
            If ex.Message.ToString.Contains("denied") Then
                updateLoggerContent("Unable to write config file")
                updateLoggerContent("Try to run as Administrator")
                updateLoggerContent("Error: " + ex.Message.ToString)
            End If
        End Try
    End Sub

    Private Sub changeGameFolder()
        Dim oldMHOFolderPath = MHOFolderPath
        selectMHOFolderManually(False, False)

        ' if smething has changed verify it
        If Not (MHOFolderPath = oldMHOFolderPath) Then
            updateLoggerContent("Verifying MHO folder from the new location...")
            Dim MHoClientExePath = MHOFolderPath + "Bin\Client\Bin32\MHOClient.exe"
            If My.Computer.FileSystem.FileExists(MHoClientExePath) Then
                updateLoggerContent("MHO folder is valid from the new location")
                gamefolderok.Enabled = True

                ' save game folder to config
                configGamePath = MHOFolderPath
                writeConfig()

                ' set game folder from strip menu
                setGameFolderStripMenu()

                ' enable patch button only if compatible
                If versionCompatible Then
                    enableStartPatchBtn()
                End If
            Else
                MsgBox("The selected location is not a valid" + vbCrLf + "Monster Hunter Online folder.", MsgBoxStyle.Critical, "Invalid Location")
                updateLoggerContent("The selected location is invalid")
            End If
        End If
    End Sub

    Private Function getIIPSFolderPath() As String
        Return MHOFolderPath + "Bin\Client\IIPS"
    End Function

    Private Function getIIPSDownloadFolderPath() As String
        Return getIIPSFolderPath() + "\iipsdownload"
    End Function

    Private Function getIIPSFileListPath() As String
        Return getIIPSFolderPath() + "\IIPSFileList.lst"
    End Function

    Private Function getMMOGDataPath() As String
        Return MHOFolderPath + "TCLS\mmog_data.xml"
    End Function

    Private Function getPatchName() As String
        Return patchName + ".ifs"
    End Function

    Private Function getPatchFilePath() As String
        Return getIIPSDownloadFolderPath() + "\" + getPatchName()
    End Function

    Private Function GetHash(ByVal strToHash As String) As String

        Dim md5Obj As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)

        bytesToHash = md5Obj.ComputeHash(bytesToHash)
        Dim strResult As New StringBuilder

        For Each b As Byte In bytesToHash
            strResult.Append(b.ToString("x2"))
        Next

        Return strResult.ToString

    End Function

    Private Sub hidePatchRom()
        IO.File.SetAttributes(getPatchFilePath(), IO.FileAttributes.Hidden + IO.FileAttributes.System)
    End Sub

    Private Sub unHidePatchRom()
        IO.File.SetAttributes(getPatchFilePath(), IO.FileAttributes.Normal)
    End Sub

    Private Sub enableStartPatchBtn()
        StartPatch.Enabled = True
        StartPatchToolStrip.Enabled = True
    End Sub

    Private Sub disableStartPatchBtn()
        StartPatch.Enabled = False
        StartPatchToolStrip.Enabled = False
    End Sub

    Private Function isGameProcessesRunning(Optional ByVal withPrompt As Boolean = True) As Boolean
        Dim mhoProces = Process.GetProcessesByName("MHOClient")
        Dim iips = Process.GetProcessesByName("iipshostapp")
        Dim running As Boolean = False
        If mhoProces.Count > 0 Then
            running = True
            If withPrompt Then
                MsgBox("Monster Hunter Online is currently running", MsgBoxStyle.Critical)
            End If
        ElseIf iips.Count > 0 Then
            iips(0).Kill()
        End If

        Return running
    End Function

    Private Sub MHOGameClientTicker_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MHOGameClientTicker.Tick
        Dim p() As Process = Process.GetProcessesByName("MHOClient")
        If p.Length > 0 Then
            gamerunningok.Enabled = True
        Else
            gamerunningok.Enabled = False

            ' make sure to kill process first
            Dim iips = Process.GetProcessesByName("iipshostapp")
            If iips.Count > 0 Then
                iips(0).Kill()
            End If
        End If
    End Sub

    Public Sub updateLoggerContent(ByVal logmsg As String)
        Try
            logbox.AppendText("> " + logmsg + vbCrLf)
            logbox.ScrollToCaret()
        Catch ex As Exception
            MsgBox("Unableto log message: " + ex.Message)
        End Try
    End Sub

    Private Sub logMessageWhenInTray(ByVal logmsg As String, Optional ByVal timeout As Integer = 1)
        If Not ShowInTaskbar Then
            NotifyIcon1.BalloonTipText = logmsg
            NotifyIcon1.ShowBalloonTip(timeout)
        End If
    End Sub

    Private Sub setGameFolderStripMenu()
        Dim gamepath As String = getConfig("gamepath")
        If gamepath Is Nothing Or gamepath = "" Then
            gamepath = MHOFolderPath
        Else
            If gamepath.Length > 22 Then
                Dim str = gamepath.Substring(0, 22)
                CPCGAMEasdasdasdToolStripMenuItem.Text = str + "..."
            End If
        End If

        CPCGAMEasdasdasdToolStripMenuItem.ToolTipText = gamepath
        CPCGAMEasdasdasdToolStripMenuItem.Visible = True
    End Sub

    Private Sub showFormFromTaskbar()
        NotifyIcon1.Visible = False
        ShowInTaskbar = True
        Me.Visible = True
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub hideFormToTaskbar()
        Try
            If MyBase.WindowState = FormWindowState.Minimized Then
                Me.Visible = False
                ShowInTaskbar = False
                NotifyIcon1.Visible = True

                ' show baloon tips
                NotifyIcon1.BalloonTipText = "v" + patchVersion + " is up and running."
                NotifyIcon1.ShowBalloonTip(5)
            End If
        Catch ex As Exception
            updateLoggerContent("Unable to minimize with error")
            updateLoggerContent(ex.Message)
        End Try

    End Sub

    Private WithEvents tim As New System.Timers.Timer
    Public Delegate Sub doSub()
    Private thingToDo As doSub

    Public Sub TimeOut(ByVal d As doSub, ByVal milliseconds As Integer)
        thingToDo = d
        tim.AutoReset = False
        tim.Interval = milliseconds
        tim.Start()
    End Sub

    Private Sub tim_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed
        Invoke(thingToDo)
    End Sub

    Private Sub ExitToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem1.Click
        Me.Close()
    End Sub

    Private Function getConfigFromLocalDataPath() As String
        If (Not System.IO.Directory.Exists(localAppDataFolder)) Then
            System.IO.Directory.CreateDirectory(localAppDataFolder)
        End If

        Return localAppDataFolder + "/config.ini"
    End Function

    Private Sub goToAddress(ByVal webAddress As String)
        Process.Start(webAddress)
    End Sub

    Private Sub DiscordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DiscordToolStripMenuItem.Click
        goToAddress(DISCORD_LINK)
    End Sub

    Private Sub WikiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WikiToolStripMenuItem.Click
        goToAddress("https://monsterhunteronline.github.io")
    End Sub

    Private Sub RedditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RedditToolStripMenuItem.Click
        goToAddress(REDDIT_LINK)
    End Sub

    Private Sub InstallationGuideToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        goToInstallationGuide()
    End Sub

    Private Sub goToInstallationGuide()
        goToAddress(REDDIT_LINK)
    End Sub

    Private Sub FacebookToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacebookToolStripMenuItem.Click
        goToAddress(FACEBOOK_LINK)
    End Sub

    Private Sub AutorunPatchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AutorunPatchToolStripMenuItem.Click
        If AutorunPatchToolStripMenuItem.Checked Then
            AutorunPatchToolStripMenuItem.Checked = False
            autostartpatch = 0
        Else
            autostartpatch = 1
            AutorunPatchToolStripMenuItem.Checked = True
        End If

        writeConfig()
    End Sub

    Private Sub AboutToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem1.Click
        About.ShowDialog()
    End Sub

    Private Sub AbouToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AbouToolStripMenuItem.Click
        About.StartPosition = FormStartPosition.CenterScreen
        About.ShowDialog()
    End Sub

    Private Sub ChangeGameFolderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangeGameFolderToolStripMenuItem.Click
        changeGameFolder()
    End Sub

    Private Sub StartPatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartPatch.Click
        startPatchProcess()
    End Sub


    Private Sub StartPatchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartPatchToolStrip.Click
        startPatchProcess()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        showFormFromTaskbar()
    End Sub

    Private Sub ShowToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowToolStripMenuItem.Click
        showFormFromTaskbar()
    End Sub

    Private Sub MinimizeToTrayToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MinimizeToTrayToolStripMenuItem.Click
        If MinimizeToTrayToolStripMenuItem.Checked Then
            MinimizeToTrayToolStripMenuItem.Checked = False
            minimizetotray = 0
        Else
            minimizetotray = 1
            MinimizeToTrayToolStripMenuItem.Checked = True
        End If

        writeConfig()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub DiscordImage_Click(sender As Object, e As EventArgs) Handles DiscordImage.Click
        goToAddress(DISCORD_LINK)
    End Sub

    Private Sub PatreonImage_Click(sender As Object, e As EventArgs) Handles PatreonImage.Click
        goToAddress(PATREON_LINK)
    End Sub

    Private Sub BitcoinImage_Click(sender As Object, e As EventArgs)
        goToAddress(BITCOIN_LINK)
    End Sub

    Private Sub PatreonToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PatreonToolStripMenuItem.Click
        goToAddress(PATREON_LINK)
    End Sub

    Private Sub PaypalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PaypalToolStripMenuItem.Click
        goToAddress(PAYPAL_LINK)
    End Sub

    Private Sub BitcoinToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BitcoinToolStripMenuItem.Click
        goToAddress(BITCOIN_LINK)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Patreon.ShowDialog()
    End Sub
End Class
