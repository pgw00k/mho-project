import fs from 'fs';
import path from 'path';
import chalk from 'chalk';
import Common from './Common';
import isUndefined from 'lodash/isUndefined';

class CsvMerger extends Common {

  constructor() {
    super();
  }

  // sourceMergeFile is the same as the downloaded csv
  merge(targetMergeFile, sourceMergeFile, outputFile, cb = false) {

    let filename = path.basename(targetMergeFile, path.extname(targetMergeFile));
    console.time(`csv merge ${filename}`);

    // by merging this, there are few thigns to note
    // if there are deleted text on target, they will be removed forever
    // if there are new text on target, they will remain

    // read target file
    fs.readFile(targetMergeFile, 'utf-8', (error, targetMerge) => {
      let csvTargetContentJSON = this.getJSONEquivalent(targetMerge);
      let csvSourceContentJSONCopy = {...csvTargetContentJSON};

      // read source file or the downloaded csv
      fs.readFile(sourceMergeFile, 'utf-8', (error, sourceMerge) => {
        let csvSourceContentJSON = this.getJSONEquivalent(sourceMerge, 2);
        let csvSourceCommentsJSON = this.getJSONEquivalent(sourceMerge, 3);

        // replace every data of target from source
        for (let hash in csvTargetContentJSON) {
          // if the hash is on the source, get it
          // and set it to target
          if (hash in csvSourceContentJSON) {
            csvTargetContentJSON[hash] = csvSourceContentJSON[hash];
          }
        }

        let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\tCOMMENTS\n';
        Object.keys(csvTargetContentJSON).forEach((hash) => {
          let comments = (hash in csvSourceCommentsJSON && !isUndefined(csvSourceCommentsJSON[hash])) ? csvSourceCommentsJSON[hash] : '<EMPTY>';
          fcontent += `${hash}\t${csvSourceContentJSONCopy[hash]}\t${csvTargetContentJSON[hash]}\t${comments}\n`;
        });

        fs.writeFile(`${outputFile}`, fcontent, (err) => {
          if (err) throw err;
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(`csv merge ${filename}`);
          cb && cb();
        });
      });
    });
  }
}

export default CsvMerger;
