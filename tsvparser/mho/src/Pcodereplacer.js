const pcodesequencereplacer = [{
  swfname: 'mhui',
  findreplace: [{
    refid: 'mh.common.ToolTip:EquipTips',
    codes: [
      { // fixed length of text for weapon sharpness ref: look for Sharpness text 斩味
        find: `      getlocal            8
      pushbyte            61
      setproperty         QName(PackageNamespace(""), "x")`,
        replace: `      getlocal            8
      pushbyte            91
      setproperty         QName(PackageNamespace(""), "x")`
      },
      { // fixed the limit of skills name on a armor equipment ref: look for soul stone suffix (狩魂石) - line 1280 usual line
        find: `      debugline           1643
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t1")
      findpropstrict      QName(PackageNamespace("utils"), "HTMLStyle")
      getproperty         QName(PackageNamespace("utils"), "HTMLStyle")
      getlocal            70
      getlocal            72
      callproperty        QName(PackageNamespace(""), "fontColorSize"), 2
      setproperty         QName(PackageNamespace(""), "htmlText")`,
        replace: `      debugline           1643
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t1")
      findpropstrict      QName(PackageNamespace("utils"), "HTMLStyle")
      getproperty         QName(PackageNamespace("utils"), "HTMLStyle")
      getlocal            70
      getlocal            72
      callproperty        QName(PackageNamespace(""), "fontColorSize"), 2
      setproperty         QName(PackageNamespace(""), "htmlText")
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t1")
      pushbyte            120
      setproperty         QName(PackageNamespace(""), "width")`
      },
      { // fixed the limit of skills name on a armor equipment ref: look for soul stone suffix (狩魂石) - line 1280 usual line
        find: `      debugline           1652
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t2")
      findpropstrict      QName(PackageNamespace("utils"), "HTMLStyle")
      getproperty         QName(PackageNamespace("utils"), "HTMLStyle")
      getlocal            70
      getlocal            72
      callproperty        QName(PackageNamespace(""), "fontColorSize"), 2
      setproperty         QName(PackageNamespace(""), "htmlText")`,
        replace: `      debugline           1652
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t2")
      findpropstrict      QName(PackageNamespace("utils"), "HTMLStyle")
      getproperty         QName(PackageNamespace("utils"), "HTMLStyle")
      getlocal            70
      getlocal            72
      callproperty        QName(PackageNamespace(""), "fontColorSize"), 2
      setproperty         QName(PackageNamespace(""), "htmlText")
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t2")
      pushint             120
      setproperty         QName(PackageNamespace(""), "width")
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t2")
      getlocal            68
      getproperty         QName(PackageNamespace(""), "t2")
      getproperty         QName(PackageNamespace(""), "x")
      pushbyte            30
      add
      setproperty         QName(PackageNamespace(""),"x")`
      },
      { // text(width) of atk,def,res on equip hover including value(x) ref: AddQualityMC function
        find: `      debugline           2909
      getlocal            5
      getlocal            5
      getproperty         QName(PackageNamespace(""), "x")
      getlocal0
      getproperty         QName(PrivateNamespace("mh.common.ToolTip:EquipTips"), "normalIconSize")
      add
      setproperty         QName(PackageNamespace(""), "x")`,
        replace: `      debugline           2909
      getlocal            5
      getlocal            5
      getproperty         QName(PackageNamespace(""), "x")
      getlocal0
      getproperty         QName(PrivateNamespace("mh.common.ToolTip:EquipTips"), "normalIconSize")
      add
      setproperty         QName(PackageNamespace(""), "x")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t1")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t1")
      getproperty         QName(PackageNamespace(""), "width")
      pushbyte            15
      add
      setproperty         QName(PackageNamespace(""), "width")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t2")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t2")
      getproperty         QName(PackageNamespace(""), "x")
      pushbyte            15
      add
      setproperty         QName(PackageNamespace(""), "x")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t2")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t2")
      getproperty         QName(PackageNamespace(""), "width")
      pushbyte            15
      add
      setproperty         QName(PackageNamespace(""), "width")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t3")
      getlocal            5
      getproperty         QName(PackageNamespace(""), "t3")
      getproperty         QName(PackageNamespace(""), "x")
      pushbyte            15
      add
      setproperty         QName(PackageNamespace(""), "x")`
    }
    ]
  }]
}];

export default pcodesequencereplacer;

// if(_loc46_ % 2 == 0)
// {
//   _loc68_ = new MC_TipTable2();
//   _loc68_.mouseChildren = false;
//   _loc68_.t1.htmlText = HTMLStyle.fontColorSize(_loc70_,_loc72_);
//   _loc68_.t1.width = 120;
//   _loc68_.t1.border = true;
//   _loc68_.t1.borderColor = 65280;
//   FontMgr.setFont(_loc68_.t1 as TextField,FontMgr.cuyuanjian);
//   _loc68_.t2.htmlText = "";
//   this.addChild(_loc68_);
//   alignV(_loc68_,-10);
//   _loc68_.y = _loc68_.y - 10;
//   _loc68_.width = _loc68_.width + 1;
// }
// else
// {
//   _loc68_.t2.htmlText = HTMLStyle.fontColorSize(_loc70_,_loc72_);
//   _loc68_.t2.width = 120;
//   _loc68_.t2.border = true;
//   _loc68_.t2.borderColor = 10627860;
//   _loc68_.t2.x = _loc68_.t2.x + 25;
//   FontMgr.setFont(_loc68_.t2 as TextField,FontMgr.cuyuanjian);
// }