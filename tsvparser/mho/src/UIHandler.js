import fs from 'fs';
import htmlparser2 from 'htmlparser2';
import xml2js from 'xml2js';
import crypto from 'crypto';
import chalk from 'chalk';
import Common from './Common';
import pcodesequencereplacer from './Pcodereplacer';
import path from 'path';
import { spawn } from 'child_process';
import jsonfile from 'jsonfile';
import copyPaste from 'copy-paste';
import fx from 'mkdir-recursive';
import range from 'lodash/range';
import chunk from 'lodash/chunk';

class UIHandler extends Common {

  constructor() {
    super();

    // set where the ffdec.jar is
    const ffdec = fs.readFileSync('ffdec.path');
    const ffdecpath = ffdec.toString().replace(/(\r\n|\n|\r)/gm, '');
    this.ffdec = path.join(ffdecpath, 'ffdec.jar');
    this.ffdecBat = path.join(ffdecpath, 'ffdec.bat');

    // set where the rabc is
    const rabc = fs.readFileSync('rabc.path');
    this.rabcpath = rabc.toString().replace(/(\r\n|\n|\r)/gm, '');

    if (!fs.existsSync(this.ffdec)) {
      this.die(`FFDec library is missing from ${ffdecpath}`);
    }

    // for xml common tags
    this.commonXMLTags = ['name', 'label', 'Text', 'Note', 'cname', 'Privilege', 'Tips'];

    // for teach
    this.keyboardsMap = {
      BULLETF1: "$bulletf1",
      BULLETF2: "$bulletf2",
      DEFENSE: "$defense",
      DODGE: "$dodge",
      FOCUS: "$focus",
      HOLSTER: "$holster",
      INTERACT: "$interact",
      MELEE1: "$melee1key",
      MELEE2: "$melee2key",
      MHITEM1: "$MhItem_1",
      MHITEM2: "$MhItem_2",
      MOVEBACK: "$moveback",
      MOVEFORWARD: "$moveforward",
      MOVELEFT: "$moveleft",
      MOVERIGHT: "$moveright",
      RAGE1: "$rage_1",
      RAGE2: "$rage_2",
      RELOAD: "$reload",
      RUSH: "$rush",
      UIHUNTERBOOK: "$ui_HunterBook",
      UIHUNTERROAD: "$ui_hunterRoad",
      UIINVENTORY: "$ui_inventoryuishortcut",
      UICOMBINATION: "$ui_manufacture",
      UIMODE: "$ui_mode",
      UIPETLIST: "$ui_petlist",
      UIHUNTERPROPERTY: "$ui_playerproperty",
      UISKILLVIDEO: "$ui_Skillvideo",
      UIWORLDMAP: "$ui_WorldMap"
    };
  }

  testffdec() {
    const child = spawn('java', ['-jar', this.ffdec, '-help']);

    child.stdout.on('data', (data) => {
      console.log(data.toString());
    });

    child.stderr.on('data', (data) => {
      console.log(`Error: ${data.toString()}`, data);
    });

    child.on('error', (err) => {
      console.log(`Failed to start child process.`, err);
    });
  }

  swf2xml(sourceDir, targetDir, all = false) {

    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    let objects = (!all) ? this.getUIReferenceContent() : this.getAllUIReferenceContent(sourceDir);
    objects.then((uijson) => {
      this.loop(uijson, (ui, next) => {
        // execute ffdec.jar for each file
        const filename = ui.name;
        let infile = path.join(sourceDir, `${filename}.swf`);

        let timename = `swf2xml ${filename}`;
        console.time(timename);

        // modify inline if custom ui
        if ('custom' in ui && ui.custom && !all) {
          let customDir = path.join(sourceDir, 'custom');
          infile = path.join(customDir, `${filename}.swf`);
        }

        const outfile = path.join(targetDir, `${filename}.xml`);
        const child = spawn('java', ['-jar', this.ffdec, '-swf2xml', infile, outfile]);

        child.stdout.on('data', (data) => {
          console.log(data.toString());
        });

        child.stderr.on('data', (data) => {
          console.log(`Error extracting: ${filename}.swf`, data.toString());
        });

        child.on('close', function (code) {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(timename);

          // now lets parse that xml file and extract the text to csv
          next();
        });

        child.on('error', (err) => {
          console.log(`Failed to start child process for ${filename}.swf`, err);
        });
      }, () => {
        console.log('All swf files has been extracted to xml');
      }, () => {
        console.log('No files to export');
      });
    }).catch(errors => console.log('Error when reading ui file', errors));
  }

  xml2swf(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    this.getUIReferenceContent().then((uijson) => {
      this.loop(uijson, (ui, next) => {
        const filename = ui.name;
        if (!('custom' in ui)) {
          // execute ffdec.jar for each file
          const infile = path.join(sourceDir, `${filename}.xml`);
          const outfile = path.join(targetDir, `${filename}.swf`);

          let timename = `xml2swf ${filename}`;
          console.time(timename);

          this.xml2swfsingle(infile, outfile, () => {
            process.stdout.write(chalk.green('OK') + ' ');
            console.timeEnd(timename);
            next();
          });
        } else {
          console.log(`${filename} is for custom abc code only`);
          next();
        }
      }, () => {
        console.log('All xml files has been imported back to swf');
      }, () => {
        console.log('No files to import');
      });
    }).catch(errors => console.log('Error when reading ui file', errors));
  }

  xml2swfsingle(infile, outfile, cb) {
    const child = spawn('java', ['-jar', this.ffdec, '-xml2swf', infile, outfile]);
    const infilename = path.basename(infile);
    const outfilename = path.basename(outfile);

    child.stdout.on('data', (data) => {
      console.log(data.toString());
    });

    child.stderr.on('data', (data) => {
      console.log(`Error importing: ${infilename}`, data);
    });

    child.on('close', function (code) {
      // console.log(`${infilename} to ${outfilename}`);
      cb && cb();
    });

    child.on('error', (err) => {
      console.log(`Failed to start child process for ${infilename}`, err);
    });
  }

  parse(filepath, cb = false, options = {}) {
    let parser = new xml2js.Parser(options);
    let content = fs.readFileSync(filepath, 'utf-8');
    if (content) {
      if ('fixCDATA' in options) {
        content = content.replace(/(\]?\]>)/ig, ']]>');
      }

      parser.parseString(content, (err, result) => {
        cb && cb(result);
      });
    }
  }

  swfxml2csv(sourceDir, targetDir, mhuifile) {

    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    this.getUIReferenceContent().then((uijson) => {
      this.loop(uijson, (ui, next) => {
        let xmlname = ui.name;
        let filepath = path.join(sourceDir, `${xmlname}.xml`);
        let targetpath = path.join(targetDir, `${xmlname}.ui.csv`);

        let timename = `swfxml2csv ${xmlname}`;
        console.time(timename);
        this.swfxml2csvsingle(filepath, targetpath, mhuifile, () => {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(timename);
          next();
        });
      }, () => {
        console.log('All swfxml files has been extracted to csv');
      }, () => {
        console.log('No files to import');
      });
    }).catch(errors => console.log('Error when reading ui file', errors));
  }

  swfxml2csvsingle(sourceFile, targetFile, mhuifile, cb) {
    let hashTable = {};
    let xmlname = path.basename(sourceFile, path.extname(sourceFile));

    // read a xml file
    this.parse(sourceFile, (result) => {
      // go to swf > tags > item
      result.swf.tags[0].item.forEach(item => {
        if ('type' in item.$ && 'initialText' in item.$ && item.$.type === 'DefineEditTextTag') {
          let value = item.$.initialText;
          let hasChinese = String(value).match(/[\u4e00-\u9fa5]/);
          if (hasChinese) {
            // create hash/signature of the text
            const hash = crypto.createHash('md5').update(value).digest("hex");

            value = value.replace(/\r\n/g, '{NL}').replace(/\r/g, '{NLD}').replace(/\t/g, '{TAB}').replace(/\s/g, '{SPC}');

            hashTable[hash] = value;
          }
        }
        // go to item > abc > constants
        else if ('type' in item.$ && item.$.type === 'DoABC2Tag') {
          if (item.abc[0].constants[0].$.type === 'AVM2ConstantPool') {
            // go to constants > constant_string > item
            const items = item.abc[0].constants[0].constant_string[0].item;
            items.forEach(nitem => {
              let hasChinese = String(nitem).match(/[\u4e00-\u9fa5]/);
              if (hasChinese) {
                // create hash/signature of the text
                const hash = crypto.createHash('md5').update(nitem).digest("hex");

                nitem = nitem.replace(/\r\n/g, '{NL}').replace(/\r/g, '{NLD}').replace(/\t/g, '{TAB}').replace(/\s/g, '{SPC}');

                hashTable[hash] = nitem;
              }
            });
          }
        }
      });

      if (mhuifile && xmlname !== 'mhui') {
        this.optimizeHashTableFromBaseMenu(hashTable, mhuifile, (newhash) => {
          console.log('optimizing hash table');
          let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\tCOMMENTS\n';
          Object.keys(newhash).forEach((hash) => {
            let comments = '<EMPTY>';
            fcontent += `${hash}\t${newhash[hash]}\t${newhash[hash]}\t${comments}\n`;
          });

          fs.writeFile(targetFile, fcontent, (err) => {
            if (err) throw err;
            cb && cb();
          });
        });
      } else {
        let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\tCOMMENTS\n';
        Object.keys(hashTable).forEach((hash) => {
          let comments = '<EMPTY>';
          fcontent += `${hash}\t${hashTable[hash]}\t${hashTable[hash]}\t${comments}\n`;
        });

        fs.writeFile(targetFile, fcontent, (err) => {
          if (err) throw err;
          cb && cb();
        });
      }
    });
  }

  optimizeHashTableFromBaseMenu(hashTable, mhuifile, cb) {
    fs.readFile(mhuifile, 'utf-8', (err, content) => {
      this.sourceJSON = this.getJSONEquivalent(content, 2);

      let newhash = Object.keys(hashTable).reduce((current, hash) => {
        if (!(hash in this.sourceJSON)) {
          current[hash] = hashTable[hash];
        }

        return current;
      }, {});

      cb && cb(newhash);
    });
  }

  csv2swfxml(sourceDir, targetDir, referenceDir) {

    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    this.getUIReferenceContent().then((uijson) => {
      this.loop(uijson, (ui, next) => {
        const filename = ui.name;
        if (!('custom' in ui)) {
          let filepath = path.join(sourceDir, `${filename}.ui.csv`);
          let reffilepath = path.join(referenceDir, `${filename}.xml`);

          // make sure ref file is present
          if (!fs.existsSync(reffilepath)) {
            console.log(`Missing reference point for \`${filename}\` from \`${referenceDir}\``);
            console.log('Going to next file');
            next();
          } else {
            let timename = `csv2swfxml ${filename}`;
            console.time(timename);

            let targetFile = path.join(targetDir, `${filename}.xml`);
            this.csv2swfxmlsingle(filepath, targetFile, reffilepath, () => {
              process.stdout.write(chalk.green('OK') + ' ');
              console.timeEnd(timename);
              next();
            });
          }
        } else {
          console.log(`${filename} is for custom abc code only`);
          next();
        }
      }, () => {
        console.log('All csv files has been imported back to swfxml');
      }, () => {
        console.log('No files to import');
      });
    }).catch(errors => console.log('Error when reading ui file', errors));
  }

  csv2swfxmlsingle(sourcecsv, targetxml, referencexml, cb) {
    let builder = new xml2js.Builder({
      // cdata: true,
      renderOpts: {
        'pretty': true,
        'indent': '  ',
        'newline': '\r\n'
      },
      xmldec: {
        'version': '1.0',
        'encoding': 'UTF-8',
        'standalone': false
      }
    });

    // read transaltion source
    fs.readFile(sourcecsv, 'utf-8', (err, content) => {
      this.sourceJSON = this.getJSONEquivalent(content, 2);

      // read a xml file (the original xml)
      this.parse(referencexml, (result) => {
        // go to swf > tags > item
        result.swf.tags[0].item.forEach((item, findex) => {
          // encode xmlMetadata
          // because the CDATA being decoded
          // if ('type' in item.$ && item.$.type === 'MetadataTag') {
          //   result.swf.tags[0].item[findex].xmlMetadata[0]._ = this.wrapCDATA(item.xmlMetadata[0]._);
          // }

          if ('initialText' in item.$) {
            // encode html strings
            let value = item.$.initialText;
            // replace any special characters

            // {AN13} is &#13;
            value = value.replace(/\r/g, '{AN13}');

            result.swf.tags[0].item[findex].$.initialText = value;
          }

          if ('type' in item.$ && 'initialText' in item.$ && item.$.type === 'DefineEditTextTag') {
            let initialTextValue = item.$.initialText;

            // if (!!~initialTextValue.indexOf('{AN13}')) {
            //   const filename = path.basename(referencexml);
            //   console.log(`${filename} has the special AN13 tag`, initialTextValue);
            // }

            // convert {AN13} to {\r} for comparing purposes
            // its because of what we did on the first if blck
            initialTextValue = initialTextValue.replace(/{AN13}/g, '\r');

            let hasChinese = String(initialTextValue).match(/[\u4e00-\u9fa5]/);
            if (hasChinese) {
              // create hash/signature of the text
              const hash = crypto.createHash('md5').update(initialTextValue).digest("hex");

              if (hash in this.sourceJSON) {
                let value = this.sourceJSON[hash];
                value = value.replace(/{NL}/g, '\r\n')
                // replace NLD to AN13
                .replace(/{NLD}/g, '{AN13}')

                .replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');

                result.swf.tags[0].item[findex].$.initialText = value;
              }
            }
          }
          // go to item > abc > constants
          else if ('type' in item.$ && item.$.type === 'DoABC2Tag') {
            if (item.abc[0].constants[0].$.type === 'AVM2ConstantPool') {
              // go to constants > constant_string > item
              const items = item.abc[0].constants[0].constant_string[0].item;
              items.forEach((nitem, lindex) => {
                if (nitem && nitem.length > 0) {
                  let hasChinese = String(nitem).match(/[\u4e00-\u9fa5]/);
                  if (hasChinese) {
                    // create hash/signature of the text
                    const hash = crypto.createHash('md5').update(nitem).digest("hex");

                    if (hash in this.sourceJSON) {
                      // console.log(this.sourceJSON[hash]);

                      let value = this.sourceJSON[hash];
                      value = value.replace(/{NL}/g, '\r\n')
                      // replace NLD to AN13
                      .replace(/{NLD}/g, '{AN13}')

                      .replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');

                      result.swf.tags[0].item[findex]
                            .abc[0].constants[0]
                              .constant_string[0]
                                .item[lindex] = value;
                    }
                  }
                }
              });
            }
          }
        });

        let xml = builder.buildObject(result);
        xml = xml
          .replace(/&lt;\/p>/g, '&lt;/p&gt;')
          .replace(/&lt;b>/g, '&lt;b&gt;')
          .replace(/&lt;\/b>/g, '&lt;/b&gt;')
          .replace(/&lt;sbr \/>/g, '&lt;sbr /&gt;')
          .replace(/&quot;>/g, '&quot;&gt;')
          .replace(/&lt;\/font>/g, '&lt;/font&gt;')

          // the following is to patch the xml2js way of building
          .replace(/&#xD;&#xD;\n/g, '&#13;\r\n') // the first &#xD here should &#13
          .replace(/&#xD;\n/g, '\r\n') // the first &#xD here should \r
          .replace(/&#xD;/g, '&#13;') // the rest of the &#xD; should be converted to &#13
          .replace(/{AN13}/g, '&#13;') // {AN13} comes from the attribute initialText, look above code

          .replace(/<\/swf>/g, '</swf>\r\n'); // replace closing swf tag

        fs.writeFile(targetxml, xml, (err) => {
          if (err) throw err;
          cb && cb();
        });
      });
    });
  }

  convertKeyboardkeys(string) {
    let haskeyboardkeywords = /\$[a-z0-9_]+/gi.test(string);
    if (haskeyboardkeywords) {
      for (let k in this.keyboardsMap) {
        let reg = new RegExp(`\\${this.keyboardsMap[k]}`, 'gi');
        string = string.replace(reg, `{KB:${k}}`);
      }
    }

    return string;
  }

  parseSpecialKeywords(string) {
    let parsed = '';
    if (string !== '') {
      var hashtmlstring = /<[a-z][\s\S]*>/i.test(string);
      if (hashtmlstring) {
        // parse html
        let line = [];
        var parser = new htmlparser2.Parser({
          onopentag: (name, attribs) => {
            switch (name) {
              case 'img':
                // its a gamepad icon
                line.push(`{GP:${attribs.src}}`);
                break;
              case 'br':
                line.push('{NL}')
                break;
              case 'b':
                line.push('{BOLD}');
                break;
              case 'font':
                let fontatrribs = [];
                for (let a in attribs) {
                  fontatrribs.push(`${a.toUpperCase()}=${attribs[a]}`)
                }

                line.push(`{FONT:${fontatrribs.join(':')}}`);
                break;
            }
          },
          ontext: (text) => {
            text = this.convertKeyboardkeys(text);
            line.push(text);
          },
          onclosetag: (name) => {
            switch (name) {
              case 'b':
                line.push('{/BOLD}');
                break;
              case 'font':
                line.push('{/FONT}');
                break;
            }
          },
          onend: () => {
            parsed = line.join('');
          }
        });
        parser.write(string);
        parser.end();
      } else {
        // replace keyboards
        string = this.convertKeyboardkeys(string);

        if (string === ' ') {
          string = '{SPACER}';
        }

        parsed = string;
      }
    }

    return parsed;
  }

  convertSpecialKeywords(string = '') {
    let parsed = string;
    if (string !== '') {
      let hasKeywords = /\{.+\}/i.test(string);
      if (hasKeywords) {
        parsed = string.replace(/({.*?})/ig, (match) => {
          // remove brackets
          let keyword = match.replace(/\{|\}/ig, '');

          // handle gamepads
          if (keyword.includes('GP:')) {
            let gamepadSrc = keyword.split(':')[1];
            return `<img src="${gamepadSrc}"/>`;
          } else if (keyword.includes('KB:')) {
            // handle keyboards
            let keyboardKey = keyword.split(':')[1];
            if (keyboardKey in this.keyboardsMap) {
              return this.keyboardsMap[keyboardKey];
            } else {
              console.log('csv2uixml teach:', chalk.red('Keyboard map error', chalk.white(keyboardKey), 'not found'));
              return match;
            }
          } else if (keyword.includes('FONT')) {
            // handle fonts
            if (keyword === '/FONT') {
              return '</font>';
            } else {
              let splits = keyword.split(':');
              // remove first occurence, the FONT keyword
              splits.shift();

              let fontatrribs = [];
              for (let s = 0; s < splits.length; s++) {
                let attribs = splits[s].split('=');
                fontatrribs.push(`${attribs[0].toLowerCase()}="${attribs[1]}"`);
              }
              return `<font ${fontatrribs.join(' ')}>`;
            }
          } else if (keyword === 'NL') {
            return '<br>';
          } else if (keyword.includes('BOLD')) {
            let boldMap = {
              'BOLD': '<b>',
              '/BOLD': '</b>'
            };
            return (keyword in boldMap) ? boldMap[keyword] : match;
          } else if (keyword === 'SPACER') {
            return ' ';
          } else {
            return match;
          }
        });
      }
    }

    return parsed;
  }

  uixml2csv(sourceDir, targetDir) {
    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    const files = fs.readdirSync(sourceDir);
    this.loop(files, (file, next) => {
      // make sure to read xml only
      if (path.extname(file) === '.xml') {
        let hashTable = {};
        let filepath = path.join(sourceDir, file);
        let xmlname = path.basename(file, path.extname(file));
        let isTeachFile = xmlname === 'teach';
        let options = (isTeachFile) ? {fixCDATA: true} : {};

        let timename = `uixml2csv ${xmlname}`;
        console.time(timename);

        // read a xml file
        this.parse(filepath, (result) => {
          const saveToHashTable = (value, keywordReplace = true) => {
            let hasChinese = String(value).match(/[\u4e00-\u9fa5]/);
            if (hasChinese) {
              // create hash/signature of the text
              const hash = crypto.createHash('md5').update(value).digest("hex");

              if (keywordReplace) {
                value = value.replace(/\r\n/g, '{NL}').replace(/\r/g, '{NLD}').replace(/\t/g, '{TAB}').replace(/\s/g, '{SPC}');
              }

              hashTable[hash] = value;
            }
          };

          // const sorter = (a, b) => {
          //   return a.localeCompare(b, undefined, {numeric: true, sensitivity: 'base'});
          // };

          // result.root.
          const traverse = (obj) => {
            if (typeof obj === 'object') {
              for (let key in obj) {
                let child = obj[key];

                if (key === '$') {
                  // check name attribute
                  if (typeof child === "object") {
                    for (let attr in child) {
                      if (!!~this.commonXMLTags.indexOf(attr) && !isTeachFile) {
                        saveToHashTable(child[attr]);
                        // console.log(1, child[attr]);
                        // obj[key][attr]['name']
                      }
                    }
                  }
                } else if (Array.isArray(child)) {
                  for (let index in child) {
                    let entry = child[index];
                    if (typeof entry === 'string') {
                      if (isTeachFile) {
                        let string = this.parseSpecialKeywords(entry);
                        if (string !== '') {
                          saveToHashTable(string, false);
                        }
                      } else {
                        saveToHashTable(entry);
                      }
                      // console.log(2, entry);
                      // obj[key][index]
                    } else {
                      traverse(entry);
                    }
                  }
                } else if (typeof child === 'object') {
                  traverse(child);
                } else {
                  if (typeof child === 'string') {
                    if (isTeachFile) {
                      let string = this.parseSpecialKeywords(child);
                      if (string !== '') {
                        saveToHashTable(string, false);
                      }
                    } else {
                      saveToHashTable(child);
                    }
                    // console.log(3, child);
                    // obj[key]
                  }
                }
              }
            }
          };

          traverse(result);

          // for debugging
          // jsonfile.writeFile(path.join(sourceDir, 'file.json'), result, {spaces: 2}, function(err) {
          //   console.error(err)
          // });

          let fcontent = 'ID_NO_EDIT\tORIGINAL_NO_EDIT\tTRANSLATE_EDIT\tCOMMENTS\n';
          Object.keys(hashTable).forEach((hash) => {
            let comments = '<EMPTY>';
            fcontent += `${hash}\t${hashTable[hash]}\t${hashTable[hash]}\t${comments}\n`;
          });

          fs.writeFile(path.join(targetDir, `${xmlname}.xml.csv`), fcontent, (err) => {
            if (err) throw err;
            process.stdout.write(chalk.green('OK') + ' ');
            console.timeEnd(timename);
            next();
          });
        }, options);
      } else {
        next();
      }
    }, () => {
      console.log('All ui xml has been extracted to csv');
    }, () => {
      console.log('No files to extract');
    });
  }

  csv2uixml(sourceDir, targetDir, referenceDir) {
    // make sure source dir is present
    if (!fs.existsSync(sourceDir)) {
      console.log(`Source directory \`${sourceDir}\` is missing`);
      process.exit(1);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    this.getUIXMLReferenceContent().then(uijson => {
      this.loop(uijson, (ui, next) => {
        let xmlname = ui.name;
        let timename = `csv2uixml ${xmlname}`;
        let targetFile = path.join(targetDir, `${xmlname}.xml`);
        let reffilepath = path.join(referenceDir, `${xmlname}.xml`);
        let csvfilepath = path.join(sourceDir, `${xmlname}.xml.csv`);
        let isTeachFile = xmlname === 'teach';
        let options = (isTeachFile) ? {fixCDATA: true} : {};

        console.time(timename);

        // make sure ref file is present
        if (!fs.existsSync(reffilepath)) {
          console.log(`Missing reference point for \`${xmlname}\` from \`${referenceDir}\``);
          next();
        } else {

          let builder = new xml2js.Builder({
            cdata: (isTeachFile) ? true : false,
            renderOpts: {
              'pretty': true,
              'indent': '  ',
              'newline': '\r\n'
            },
            headless: ui.headless
          });

          // read transaltion source
          fs.readFile(csvfilepath, 'utf-8', (err, content) => {
            let sourceJSON = this.getJSONEquivalent(content, 2);

            // read a xml file (the original xml)
            this.parse(reffilepath, (result) => {
              const getFromHashTable = (value, keywordReplace = true) => {
                let hasChinese = String(value).match(/[\u4e00-\u9fa5]/);
                if (hasChinese) {
                  // create hash/signature of the text
                  const hash = crypto.createHash('md5').update(value).digest("hex");

                  if (hash in sourceJSON) {
                    // console.log(sourceJSON[hash]);
                    value = sourceJSON[hash];

                    if (keywordReplace) {
                      value = value.replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r').replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');
                    }
                  }
                }

                return value;
              };

              // result.root.
              const traverse = (obj) => {
                if (typeof obj === 'object') {
                  for (let key in obj) {
                    let child = obj[key];

                    if (key === '$') {
                      // check name attribute
                      if (typeof child === "object") {
                        for (let attr in child) {
                          if (!!~this.commonXMLTags.indexOf(attr) && !isTeachFile) {
                            // console.log(1, obj[key][attr]);
                            obj[key][attr] = getFromHashTable(child[attr]);
                          }
                        }
                      }
                    } else if (Array.isArray(child)) {
                      for (let index in child) {
                        let entry = child[index];
                        if (typeof entry === 'string') {
                          // console.log(2, obj[key][index]);
                          if (isTeachFile) {
                            // parse the html string
                            let string = this.parseSpecialKeywords(entry);
                            // get the transaltion from hash table
                            string = getFromHashTable(string, false);
                            // convert keywords to html string
                            obj[key][index] = this.convertSpecialKeywords(string);
                          } else {
                            obj[key][index] = getFromHashTable(entry);
                          }
                        } else {
                          traverse(entry);
                        }
                      }
                    } else if (typeof child === 'object') {
                      traverse(child);
                    } else {
                      if (typeof child === 'string') {
                        // console.log(3, obj[key]);
                        if (isTeachFile) {
                          // parse the html string
                          let string = this.parseSpecialKeywords(child);
                          // get the transaltion from hash table
                          string = getFromHashTable(string, false);
                          // convert keywords to html string
                          obj[key] = this.convertSpecialKeywords(string);
                        } else {
                          obj[key] = getFromHashTable(child);
                        }
                      }
                    }
                  }
                }

                return obj;
              };

              const finalResult = traverse(result);

              // // for debugging
              // jsonfile.writeFile(path.join(sourceDir, 'file.json'), result, {spaces: 2}, function(err) {
              //   console.error(err)
              // });

              let xml = builder.buildObject(finalResult);
              fs.writeFile(targetFile, xml, (err) => {
                if (err) throw err;
                process.stdout.write(chalk.green('OK') + ' ');
                console.timeEnd(timename);
                next();
              });
            }, options);
          });

        }
      }, () => {
        console.log('All csv has been reimported back to xml');
      }, () => {
        console.log('No files to import');
      });
    });
  }


  rimraf(dir_path) {
    if (fs.existsSync(dir_path)) {
      fs.readdirSync(dir_path).forEach((entry) => {
        var entry_path = path.join(dir_path, entry);
        if (fs.lstatSync(entry_path).isDirectory()) {
          this.rimraf(entry_path);
        } else {
          fs.unlinkSync(entry_path);
        }
      });

      fs.rmdirSync(dir_path);
    }
  }

  /**
   * Export abc files from swf
   * and immediately dissamble abc files
   * This will create file-0.abc ... file-N.abc
   * ./abcexport file.swf
   */
  exportswfabc(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    const files = fs.readdirSync(sourceDir);
    this.loop(files, (file, next) => {
      // make sure the file is swf
      if (!!~file.indexOf('.swf')) {
        // create directory and copy the file
        const filename = path.basename(file, path.extname(file));

        // if (filename !== 'mhui') {
        //   return next();
        // }

        let timename = `exportswfabc ${filename}`;
        console.time(timename);

        // abc path is the folder of the swf file
        const abcpath = path.join(targetDir, filename);
        this.rimraf(abcpath);
        if (!fs.existsSync(abcpath)) {
          fs.mkdirSync(abcpath);
        }

        // copy the swf file to abcpath
        let fromswf = path.join(sourceDir, file);
        let toswf = path.join(abcpath, file);
        this.copyFile(fromswf, toswf, () => {
          // when file is copied
          // execute disassembler to the file
          const abcexport = path.join(this.rabcpath, 'abcexport.exe');
          const child = spawn(abcexport, [toswf]);

          child.stdout.on('data', (data) => {
            console.log(data.toString());
          });

          child.stderr.on('data', (data) => {
            console.log(`Error exporting abc of: ${filename}`);
          });

          child.on('close', (code) => {
            // console.log(`${filename} successfully exported abc files`);

            // remove mhui.swf
            fs.unlinkSync(toswf);

            // read each file and remove files that doesn't have any chinese text
            // console.log(`filtering abc files without chinese text`);
            const abcfiles = fs.readdirSync(abcpath);
            this.loop(abcfiles, (file, abcnext) => {
              if (path.extname(file) === '.abc') {
                let abcfile = path.join(abcpath, file);
                fs.readFile(abcfile, 'utf-8', (error, abcsource) => {
                  if (error) {
                    console.log('Unable to remove', abcfile);
                  }

                  let hasChinese = String(abcsource).match(/[\u4e00-\u9fa5]/);
                  if (hasChinese) {
                    // disassemble file
                    this.disassembleabc(abcfile, () => {
                      fs.unlink(abcfile, abcnext);
                    });
                  } else {
                    fs.unlink(abcfile, abcnext);
                  }
                });
              }
            }, () => {
              process.stdout.write(chalk.green('OK') + ' ');
              console.timeEnd(timename);
              next();
            }, () => {
              console.log('No abc files to filter');
            });
          });

          child.on('error', (err) => {
            console.log(`Failed to start child process for ${filename}`, err);
          });
        });
      } else {
        next();
      }
    }, () => {
      console.log('All swf files has been exported to abc');
    }, () => {
      console.log('No files to export');
    });
  }

  /**
   * dissamble abc files to their respective folders
   * This will create a file-0 directory, which will contain file-0.main.asasm
   * ./rabcdasm file-0.abc
   */
  disassembleabc(abcfilepath, cb) {
    const abcdisassembler = path.join(this.rabcpath, 'rabcdasm.exe');
    const child = spawn(abcdisassembler, [abcfilepath]);
    const filename = path.basename(abcfilepath, path.extname(abcfilepath));

    child.stdout.on('data', (data) => {
      console.log(data.toString());
    });

    child.stderr.on('data', (data) => {
      console.log(`Error disassembling abc : ${filename}`);
    });

    child.on('close', (code) => {
      // console.log(`${filename} successfully disassembled`);
      cb && cb();
    });

    child.on('error', (err) => {
      console.log(`Failed to start child process for ${filename}`, err);
    });
  }

  /**
   * assemble the .asasm files back from the created folders
   * ./rabcasm file-0/file-0.main.asasm
   */
  assembleabc(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // empty for now - seems to working
    // when mhui is saved before editing
    let swfassembledpathtoinclude = [{}];
    // let swfassembledpathtoinclude = [{
    //   swfname: 'mhui',
    //   paths: [
    //     'mh/common/',
    //     'mh/view/',
    //     'mh/model/',
    //     'mh/controller/',
    //   ]
    // }];

    const swfdirs = fs.readdirSync(sourceDir).filter(file => fs.lstatSync(path.join(sourceDir, file)).isDirectory());
    this.loop(swfdirs, (swfdir, next) => {
      // read abc dirs
      let skipfiles = [];
      let swfdirabc = path.join(sourceDir, swfdir);

      let timename = `assembleabc ${swfdir}`;
      console.time(timename);

      const abcdirs = fs.readdirSync(swfdirabc).filter(file => fs.lstatSync(path.join(swfdirabc, file)).isDirectory());
      this.loop(abcdirs, (abcdir, abcnext) => {
        // assemble
        const abcassembler = path.join(this.rabcpath, 'rabcasm.exe');
        const mainasm = path.join(swfdirabc, abcdir, `${abcdir}.main.asasm`);
        // console.log(mainasm);
        let includeFile = false;
        this.loop(swfassembledpathtoinclude, (swf, snext) => {
          if (swfdir === swf.swfname) {
            fs.readFile(mainasm, 'utf-8', (error, mainasmSource) => {
              this.loop(swf.paths, (classpath, cnext) => {
                if (!!~mainasmSource.indexOf(`#include "${classpath}`)) {
                console.log(classpath);
                  includeFile = true;
                }

                cnext();
              }, snext);
            });
          } else {
            // if swf not in include paths
            // just add it by default
            includeFile = true;
            snext();
          }
        }, () => {
          if (includeFile) {
            const child = spawn(abcassembler, [mainasm]);

            child.stdout.on('data', (data) => {
              console.log(data.toString());
            });

            child.stderr.on('data', (data) => {
              console.log(`Error assembling abc dir : ${abcdir}`);
            });

            child.on('close', (code) => {
              // console.log(`${abcdir} successfully assembled`);
              // copy to target dir
              const abcname = `${abcdir}.main.abc`;
              const assembledabc = path.join(swfdirabc, abcdir, abcname);
              if (fs.existsSync(assembledabc)) {
                // create directory to targetdir
                let abcreadydir = path.join(targetDir, swfdir);
                if (!fs.existsSync(abcreadydir)) {
                  fs.mkdirSync(abcreadydir);
                }

                this.copyFile(assembledabc, path.join(abcreadydir, abcname), () => {
                  // remove the ref
                  fs.unlink(assembledabc, abcnext);
                });
              } else {
                console.log('Unable to copy', abcname);
              }
            });

            child.on('error', (err) => {
              console.log(`Failed to start child process for ${abcdir}`, err);
            });
          } else {
            console.log('Skipping ', mainasm);
            skipfiles.push(abcdir);
            abcnext();
          }
        });
      }, () => {
        if (skipfiles.length > 0) {
          console.log(swfdir, 'skipped files', skipfiles.length);
        }

        process.stdout.write(chalk.green('OK') + ' ');
        console.timeEnd(timename);
        next();
      }, () => {
        console.log('No abcdirs to assemble');
      });
    }, () => {
      console.log('All abc files has been assembled');
    }, () => {
      console.log('No dir to assemble');
    });
  }

  /**
   * Replace the abc that corresponds to the swf
   * ./abcreplace file.swf 0 file-0/file-0.main.abc
   * The second abcreplace argument represents the index of the ABC block in the SWF file,
   * and corresponds to the number in the filename created by abcexport.
   */
  replaceswfabc(sourceDir, targetDir, referenceDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    if (!fs.existsSync(referenceDir)) {
      this.die(`Reference directory \`${referenceDir}\` is missing`);
    }

    const swfdirs = fs.readdirSync(sourceDir).filter(file => fs.lstatSync(path.join(sourceDir, file)).isDirectory());
    this.loop(swfdirs, (swfdir, next) => {
      let timename = `replaceswfabc ${swfdir}`;
      console.time(timename);

      // cipy the swf file from reference
      // to targetdir
      let refswf = path.join(referenceDir, `${swfdir}.swf`);
      let targetswf = path.join(targetDir, `${swfdir}.swf`);
      this.copyFile(refswf, targetswf, () => {
        // read the main abc files
        let swfdirabc = path.join(sourceDir, swfdir);
        const abcfiles = fs.readdirSync(swfdirabc);
        this.loop(abcfiles, (abcfile, abcnext) => {
          let abcmainfile = path.join(swfdirabc, abcfile);
          let abcindex = path.basename(abcmainfile).split('-')[1].split('.main')[0];

          // replace abc into the swf
          const replacer = path.join(this.rabcpath, 'abcreplace.exe');
          const child = spawn(replacer, [targetswf, abcindex, abcmainfile]);

          child.stdout.on('data', (data) => {
            console.log(data.toString());
          });

          child.stderr.on('data', (data) => {
            console.log(`Error replacing abc : ${abcfile}`);
          });

          child.on('close', (code) => {
            // console.log(`${abcfile} successfully replaced`);
            abcnext();
          });

          child.on('error', (err) => {
            console.log(`Failed to start child process for ${abcfile}`, err);
          });

        }, () => {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(timename);
          next()
        }, () => {
          console.log('No abc files to replace from');
        });
      });
    }, () => {
      console.log('Abc has been replaced in all swf files');
    }, () => {
      console.log('No dir to replace from');
    });
  }

  translateabc(targetDir, referenceDir) {

    // console.time('translateabc');
    const swfdirs = fs.readdirSync(targetDir).filter(file => fs.lstatSync(path.join(targetDir, file)).isDirectory());
    this.loop(swfdirs, (swfdir, next) => {

      // if (swfdir !== 'mhui') {
      //   next();
      // }

      let timename = `translateabc ${swfdir}`;
      console.time(timename);

      // read the source translation
      let sourcetranslationFile = path.join(referenceDir, `${swfdir}.ui.csv`);
      // console.log('sourcetranslation', sourcetranslationFile);
      fs.readFile(sourcetranslationFile, 'utf-8', (error, sourceTrans) => {
        let csvSourceContentOriginalJSON = this.getJSONEquivalent(sourceTrans, 1);
        let csvSourceContentTranslatedJSON = this.getJSONEquivalent(sourceTrans, 2);

        // read abc dirs
        let swfdirabc = path.join(targetDir, swfdir);
        const abcdirs = fs.readdirSync(swfdirabc).filter(file => fs.lstatSync(path.join(swfdirabc, file)).isDirectory());
        this.loop(abcdirs, (abcdir, abcdrnext) => {
          // read all files from source dir
          let abcfiles = this.readDirRecursive(path.join(swfdirabc, abcdir));
          this.loop(abcfiles, (abcfile, abcnext) => {
            // only allow class.asasm
            if (!!~abcfile.indexOf('class.asasm')) {
              fs.readFile(abcfile, 'utf-8', (error, sourceFile) => {

                // replace some code if any
                this.loop(pcodesequencereplacer, (seqreplacerobj, srnext) => {
                  if (swfdir === seqreplacerobj.swfname) {
                    this.loop(seqreplacerobj.findreplace, ({refid, codes}, frnext) => {
                      let look = sourceFile.search(`refid "${refid}"`);
                      if (!!~look) {
                        this.loop(codes, ({find, replace}, rnext) => {
                          let replaced = this.replaceAll(sourceFile, find, replace);

                          if (replaced !== sourceFile) {
                            sourceFile = replaced;
                            console.log('code has been replaced', abcfile);
                          } else {
                            this.die(`translateabc ${swfdir} error: code not find pcode! It could've been moved ${find} ${abcfile}`);
                          }

                          rnext();
                        }, frnext);
                      } else {
                        frnext();
                      }
                    }, srnext);
                  } else {
                    // console.log('Skipping file', abcfile, 'for code replacer');
                    srnext();
                  }
                }, () => {
                  // console.log('reading sourceFile', abcfile);
                  let lines = sourceFile.split(/\r?\n/g);
                  if (!sourceFile) {
                    this.die('abc file', sourceFile, 'is empty');
                  }

                  // replace every data of target from source
                  let isDirty = false;

                  // compare every line of string to the source file
                  // read source file
                  for (let x = 0; x < lines.length; x++) {
                    let sourceLine = lines[x];
                    let hasChinese = String(sourceLine).match(/[\u4e00-\u9fa5]/);
                    if (hasChinese) {

                      for (let hash in csvSourceContentOriginalJSON) {
                        let originalText = csvSourceContentOriginalJSON[hash].replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r').replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');
                        let translatedText = csvSourceContentTranslatedJSON[hash].replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r').replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');
                        let replaced = this.replaceAll(sourceLine, `"${originalText}"`, `"${translatedText}"`);
                        if (replaced !== sourceLine) {
                          lines[x] = replaced;
                          // console.log('replaced', originalText, 'with', translatedText);
                          isDirty = true;
                        }
                      }
                    }
                  }

                  if (isDirty) {
                    // console.log(`modifying ${path.basename(abcfile)}`);
                    fs.writeFile(abcfile, lines.join('\n'), (err) => {
                      if (err) throw err;
                      abcnext();
                    });
                  } else {
                    abcnext();
                  }
                });
              });
            } else {
              abcnext();
            }
          }, () => {
            abcdrnext();
          }, () => {
            console.log('No files dir to translate');
          });
        }, () => {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(timename);
          next();
        }, () => {
          console.log('No abc dir to translate');
        });

      });

    }, () => {
      console.log('All abc files has been translated');
    }, () => {
      console.log('No dir to translate');
    });
  }

  exportTextcodeFile(filename, sourceDir, targetFolder, next) {
    // create targetDir if not present
    if (!fs.existsSync(targetFolder)){
      fx.mkdirSync(targetFolder);
    }

    console.log(`exporting ${filename}.swf`);
    let filecount = 0;
    const infile = path.join(sourceDir, `${filename}.swf`);
    const child = spawn('java', ['-jar', this.ffdec, '-format', 'text:formatted', '-export', 'text', targetFolder, infile]);

    child.stdout.on('data', (data) => {
      // console.log(data.toString());
      filecount++;
    });

    child.stderr.on('data', (data) => {
      console.log(`Error exporting: ${filename}.swf`);
    });

    child.on('close', function (code) {
      console.log(`${filename}.swf text exported to ${targetFolder} with ${filecount} files`);
      next();
    });

    child.on('error', (err) => {
      console.log(`Failed to start child process for ${filename}.swf`, err);
    });
  }

  exportswftextcode(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // read all files
    const files = fs.readdirSync(sourceDir);
    this.loop(files, (file, next) => {
      // make sure the file is xml otherwise skip
      if (!!~file.indexOf('.swf')) {
        // execute ffdec.jar for each file
        const filename = file.split('.')[0];

        let timename = `exportswftextcode ${filename}`;
        console.time(timename);

        // remove existing directory if any
        const targetFolder = path.join(targetDir, filename);
        if (fs.existsSync(targetFolder)) {
          const rmspawn = spawn('rm', ['-rf', targetFolder]);
          rmspawn.stderr.on('data', (data) => {
            console.log(`Error removing: ${targetFolder}`);
          });

          rmspawn.on('close', (code) => {
            console.log(`${targetFolder} has been removed`);
            this.exportTextcodeFile(filename, sourceDir, targetFolder, () => {
              process.stdout.write(chalk.green('OK') + ' ');
              console.timeEnd(timename);
              next();
            });
          });

          rmspawn.on('error', (err) => {
            console.log(`Error removing: ${targetFolder}`, err);
          });
        } else {
          this.exportTextcodeFile(filename, sourceDir, targetFolder, () => {
            process.stdout.write(chalk.green('OK') + ' ');
            console.timeEnd(timename);
            next();
          });
        }
      } else {
        next();
      }
    }, () => {
      // remove files with no chinese text
      let textcodes = this.readDirRecursive(targetDir);
      this.loop(textcodes, (textcode, tnext) => {
        fs.readFile(textcode, 'utf-8', (error, textcodesource) => {
          if (error) {
            console.log('Unable to remove', textcode);
          }

          let hasChinese = String(textcodesource).match(/[\u4e00-\u9fa5]/);
          if (!hasChinese) {
            fs.unlink(textcode, tnext);
          } else {
            tnext();
          }
        });
      });

      console.log('All swf has been extracted their textcode');
    }, () => {
      console.log('No files to export');
    });
  }

  textcode2swf(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fx.mkdirSync(targetDir);
    }

    let files = fs.readdirSync(sourceDir);
    this.loop(files, (pcodedir, next) => {
      let sourcepcodedir = path.join(sourceDir, pcodedir);
      let filename = pcodedir;
      if (fs.statSync(sourcepcodedir).isDirectory()) {
        // console.log(`${filename} dir importing to ${filename}.swf`);
        let timename = `textcode2swf ${filename}`;
        console.time(timename);

        const infile = path.join(targetDir, `${filename}.swf`);
        const outfile = path.join(targetDir, `${filename}.swf`);
        const textsFolder = path.join(sourceDir, filename);

        let texts = fs.readdirSync(textsFolder);

        // use chunks, to avoid error "The command line is too long"
        this.loop(chunk(texts, 150), (textchunk, lnext) => {
          let cmd = ['-replace', infile, outfile];

          this.loop(textchunk, (text, tnext) => {
            let characterId = path.basename(text, path.extname(text));
            let textfile = path.join(textsFolder, text);
            // console.log(characterId, textfile);
            cmd.push(Number(characterId));
            cmd.push(textfile);
            tnext();
          }, () => {
            // console.log(cmd);
            const child = spawn(this.ffdecBat, cmd);

            child.stdout.on('data', (data) => {
              console.log(data.toString());
            });

            child.stderr.on('data', (data) => {
              console.log(`Error importing to: ${filename}.swf`, data.toString());
            });

            child.on('close', function (code) {
              // console.log(`${filename} dir imported to ${filename}.swf`);
              console.log(timename, 'chunk', textchunk.length, 'imported');
              lnext();
            });

            child.on('error', (err) => {
              console.log(`Failed to start child process for ${filename}`, err);
            });
          }, () => {
            console.log(`No txt to import for ${filename}`);
            lnext();
          });
        }, () => {
          process.stdout.write(chalk.green('OK') + ' ');
          console.timeEnd(timename);
          next();
        }, () => {
          console.log(`No txt to import for ${filename}`);
          next();
        });
      } else {
        next();
      }
    }, () => {
      console.log('All textcode has been reimported back to swf');
    }, () => {
      console.log('No files to import');
    });
  }

  exportImagecodeFile(filename, sourceDir, targetFolder, next) {
    // create targetDir if not present
    if (!fs.existsSync(targetFolder)){
      fx.mkdirSync(targetFolder);
    }

    console.log(`exporting ${filename}.swf`);
    let filecount = 0;
    const infile = path.join(sourceDir, `${filename}.swf`);
    const child = spawn('java', ['-jar', this.ffdec, '-format', 'image:png', '-export', 'image', targetFolder, infile]);

    child.stdout.on('data', (data) => {
      // console.log(data.toString());
      filecount++;
    });

    child.stderr.on('data', (data) => {
      console.log(`Error exporting: ${filename}.swf`);
    });

    child.on('close', function (code) {
      console.log(`${filename}.swf text exported with ${filecount} files`);
      next();
    });

    child.on('error', (err) => {
      console.log(`Failed to start child process for ${filename}.swf`, err);
    });
  }

  swf2imagecode(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fx.mkdirSync(targetDir);
    }

    // read all files
    const files = fs.readdirSync(sourceDir);
    this.loop(files, (file, next) => {
      // make sure the file is swf otherwise skip
      if (path.extname(file) === '.swf') {
        // execute ffdec.jar for each file
        const filename = file.split('.')[0];
        console.log(filename);

        let timename = `swf2imagecode ${filename}`;
        console.time(timename);

        // remove existing directory if any
        const targetFolder = path.join(targetDir, filename);
        if (fs.existsSync(targetFolder)) {
          console.log(`${targetFolder} already existed`);
          next();
          // const rmspawn = spawn('rm', ['-rf', targetFolder]);
          // rmspawn.stderr.on('data', (data) => {
          //   console.log(`Error removing: ${targetFolder}`);
          // });

          // rmspawn.on('close', (code) => {
          //   console.log(`${targetFolder} has been removed`);
          //   this.exportImagecodeFile(filename, sourceDir, targetFolder, () => {
          //     console.timeEnd(timename);
          //     next();
          //   });
          // });

          // rmspawn.on('error', (err) => {
          //   console.log(`Error removing: ${targetFolder}`, err);
          // });
        } else {
          this.exportImagecodeFile(filename, sourceDir, targetFolder, () => {
            process.stdout.write(chalk.green('OK') + ' ');
            console.timeEnd(timename);
            next();
          });
        }
      } else {
        next();
      }
    }, () => {
      // // remove files with no chinese text
      // let textcodes = this.readDirRecursive(targetDir);
      // this.loop(textcodes, (textcode, tnext) => {
      //   fs.readFile(textcode, 'utf-8', (error, textcodesource) => {
      //     if (error) {
      //       console.log('Unable to remove', textcode);
      //     }

      //     let hasChinese = String(textcodesource).match(/[\u4e00-\u9fa5]/);
      //     if (!hasChinese) {
      //       fs.unlink(textcode, tnext);
      //     } else {
      //       tnext();
      //     }
      //   });
      // });

      console.log('All swf has been extracted their imgecode');
    }, () => {
      console.log('No files to export');
    });
  }

  imagecode2swf(sourceDir, targetDir) {
    if (!fs.existsSync(sourceDir)) {
      this.die(`Source directory \`${sourceDir}\` is missing`);
    }

    // create targetDir if not present
    if (!fs.existsSync(targetDir)){
      fx.mkdirSync(targetDir);
    }

    let files = fs.readdirSync(sourceDir);
    this.loop(files, (imagecodedir, next) => {
      let sourceimagecodedir = path.join(sourceDir, imagecodedir);
      // console.log(sourceimagecodedir);
      let filename = imagecodedir;
      if (fs.statSync(sourceimagecodedir).isDirectory()) {
        // console.log(`${filename} dir importing to ${filename}.swf`);
        let timename = `imagecode2swf ${filename}`;
        console.time(timename);

        const infile = path.join(targetDir, `${filename}.swf`);
        const outfile = path.join(targetDir, `${filename}.swf`);
        const imagesFolder = path.join(sourceDir, filename, 'images');

        if (!fs.existsSync(infile)) {
          console.log('Cannot locate file', infile);
          next();
        }

        let images = fs.readdirSync(imagesFolder).filter(image => path.extname(image) === '.png');

        let cmd = ['-replace', infile, outfile];

        this.loop(images, (image, inext) => {
          let characterId = path.basename(image, path.extname(image));

          // if contains underscore, definitely its not a whole character ID
          if (!!~characterId.indexOf('_') && isNaN(characterId)) {
            characterId = characterId.split('_')[0];
          }

          let imagefile = path.join(imagesFolder, image);
          cmd.push(Number(characterId));
          cmd.push(imagefile);
          cmd.push('lossless2');
          inext();
        }, () => {
          // console.log(cmd);
          const child = spawn(this.ffdecBat, cmd);

          child.stdout.on('data', (data) => {
            console.log(data.toString());
          });

          child.stderr.on('data', (data) => {
            console.log(`Error importing to: ${filename}.swf`, data.toString());
          });

          child.on('close', function (code) {
            // console.log(`${filename} dir imported to ${filename}.swf`);
            process.stdout.write(chalk.green('OK') + ' ');
            console.timeEnd(timename);
            next();
          });

          child.on('error', (err) => {
            console.log(`Failed to start child process for ${filename}`, err);
          });
        }, () => {
          console.log(`No image to import for ${filename}`);
          next();
        });
      }
    }, () => {
      console.log('All imagecode has been reimported back to swf');
    }, () => {
      console.log('No files to import');
    });
  }

  translatetextcode(source, sourcetranslation) {

    fs.readdirSync(source).map(pcodedir => {
      let sourcepcodedir = path.join(source, pcodedir);
      if (fs.statSync(sourcepcodedir).isDirectory()) {

        // read the source translation
        let sourcetranslationFile = path.join(sourcetranslation, `${pcodedir}.ui.csv`);
        // console.log('sourcetranslation', sourcetranslationFile);
        fs.readFile(sourcetranslationFile, 'utf-8', (error, sourceTrans) => {
          let csvSourceContentOriginalJSON = this.getJSONEquivalent(sourceTrans, 1);
          let csvSourceContentTranslatedJSON = this.getJSONEquivalent(sourceTrans, 2);

          // read all files from source dir
          let files = this.readDirRecursive(sourcepcodedir);
          this.loop(files, (filepath, next) => {
            // console.log(filepath);
            let filename = path.basename(filepath, path.extname(filepath));

            fs.readFile(filepath, 'utf-8', (error, sourceFile) => {
              let lines = sourceFile.split(/\r?\n/g);

              // replace every data of target from source
              let isDirty = false;

              // compare every line of string to the source file
              // read source file
              for (let x = 0; x < lines.length; x++) {
                let sourceLine = lines[x];
                let hasChinese = String(sourceLine).match(/[\u4e00-\u9fa5]/);
                if (hasChinese) {

                  for (let hash in csvSourceContentOriginalJSON) {
                    let originalText = csvSourceContentOriginalJSON[hash].replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r').replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');
                    let translatedText = csvSourceContentTranslatedJSON[hash].replace(/{NL}/g, '\r\n').replace(/{NLD}/g, '\r').replace(/{TAB}/g, '\t').replace(/{SPC}/g, ' ');

                    // modify sourceline because it comes from text extract formatted
                    let newsrcline = sourceLine.substr(1);
                    let replaced = this.replaceAll(newsrcline, originalText, translatedText, false, true);

                    if (replaced !== newsrcline) {
                      lines[x] = ']' + replaced; //text formatted has a ] on the first char
                      // console.log('replaced', originalText, 'with', translatedText);
                      isDirty = true;
                    }
                  }
                }
              }

              if (isDirty) {
                let timename = `translatetextcode ${pcodedir} - ${filename}`;
                console.time(timename);
                // console.log(`modifying ${filepath}`);
                // \r\n is essential so ffdec can detect if formatted or not
                // it happens from ffdec TextImporter.java
                fs.writeFile(filepath, lines.join('\r\n'), (err) => {
                  if (err) throw err;
                  process.stdout.write(chalk.green('OK') + ' ');
                  console.timeEnd(timename);
                  next();
                });
              } else {
                next();
              }
            });
          });
        });
      }
    });
  }
}

export default UIHandler;
