import fs from 'fs';
import path from 'path';
import { spawn } from 'child_process';
import Common from './Common';

// plugin list
import DownloadPlugin from './plugins/Download';
import EncDecPlugin from './plugins/EncDec';
import TsvPlugin from './plugins/Tsv';
import CsvPlugin from './plugins/Csv';
import UiPlugin from './plugins/Ui';
import IfsPlugin from './plugins/Ifs';
import ActivitiesPlugin from './plugins/Activities';
const pluginList = [
  DownloadPlugin,
  EncDecPlugin,
  TsvPlugin,
  CsvPlugin,
  UiPlugin,
  IfsPlugin,
  ActivitiesPlugin
];

class MHO extends Common {

  constructor() {
    super();

    // method
    this.props = {
      method: '',
      tsvname: '',
      tsvref: {}
    };

    this.plugins = pluginList.map(list => {
      return new list();
    });
  }

  isLocalMethod(method) {
    return (typeof this[method] === 'function') ? true : false;
  }

  isPluginMethod(method) {
    let exist = false
    for (let x = 0; x < this.plugins.length; x++) {
      if (typeof this.plugins[x][method] === 'function') {
        exist = true;
        break;
      }
    }
    return exist;
  }

  isMethodExist(method) {
    return this.isLocalMethod(method) || this.isPluginMethod(method);
  }

  callMethod(method) {
    if (this.isLocalMethod(method)) {
      // console.log('calling local method');
      this[method]();
    } else {
      if (this.isPluginMethod(method)) {
        for (let x = 0; x < this.plugins.length; x++) {
          if (typeof this.plugins[x][method] === 'function') {
            this.plugins[x][method]();
            break;
          }
        }
        // console.log('Calling plugin method');
      }
    }
  }

  /**
   * list the names that exceed char limits
   */
  charlimits() {
    const sourceFileCsv = process.argv[3];
    let maxlen = process.argv[4];

    if (maxlen === undefined) {
      maxlen = 31;
    }

    // make sure source dir is exist
    if (!fs.existsSync(sourceFileCsv)) {
      this.die('Source file is missing');
    }

    fs.readFile(sourceFileCsv, 'utf-8', (err, content) => {
      this.sourceJSON = this.getJSONEquivalent(content, 2);
      const filename= path.basename(sourceFileCsv, path.extname(sourceFileCsv));

      this.getLimitReferenceContent().then(json => {
        const tsvlimit = json.find(t => t.name === filename);


        Object.keys(this.sourceJSON).forEach((hash) => {
          if (this.sourceJSON[hash].indexOf('<FONT') === -1) {
            if (this.sourceJSON[hash].length > maxlen) {
              console.log(this.sourceJSON[hash]);
            }
          }
        })
      });
    });
  }

  /**
   * Remove dat files that aren't
   * included inside the tsv.json
   */
  filterenc() {
    this.getTSVReferenceContent().then((tsvjson) => {
      const targetDir = 'staticdata_enc/';
      // wether to remove xml files only
      const xmlOnly = true;

      // read enc folder
      // remove files that are not present inside tsvjson
      console.time('filterenc')
      const files = fs.readdirSync(targetDir);
      this.loop(files, (file, next) => {
        if (xmlOnly) {
          if (path.extname(file) === '.xml') {
            fs.unlink(`${targetDir}/${file}`, next);
          } else {
            next();
          }
        } else {
          const currTsvname = file.replace('.dat', '');
          const tsvref = tsvjson.find(tsv => {
            return tsv.name === currTsvname && !('disabled' in tsv)
          });

          if (!tsvref) {
            fs.unlink(`${targetDir}/${file}`, next);
          } else {
            next();
          }
        }
      }, () => {
        console.timeEnd('filterenc');
      }, () => {
        console.error("Could not list the directory: ", sourceDir);
        process.exit(1);
      });
    });
  }

  /**
   * Copy the enc files from source folder
   * to target folder `staticdata_enc`
   * this will only copy the files that are present
   * to the target folder
   * node mho --recopyenc [source dir] [target dir]
   */
  recopyenc() {
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    // make sure source dir is exist
    if (!fs.existsSync(sourceDir)) {
      this.die('Source directory is missing');
    }

    // now read all files from the targetDir
    // this will be the reference of the files
    // to be copied
    // later on all of this files will be replaced
    const filesTarget = fs.readdirSync(targetDir);
    console.log(`Copying ${filesTarget.length} files`);
    this.loop(filesTarget, (file, next) => {
      const sourceFile = path.join(sourceDir, file);
      const targetFile = path.join(targetDir, file);
      console.log('Copying', file, '...');
      this.copyFile(sourceFile, targetFile, next);
    });

  }

  /**
   * Copy the enc files based on the tsv.json
   * to target folder `staticdata_enc`
   */
  recopyencfromjson() {
    const targetDir = process.argv[3];

    if (targetDir === undefined) {
      console.log("TargetDir is missing");
      return false;
    }

    // read mho.path files contans where mho is installed
    if (!fs.existsSync('mho.path')) {
      console.log('mho.path is missing');
      return false;
    }

    // make sure target dir is exist
    if (!fs.existsSync(targetDir)) {
      fs.mkdirSync(targetDir);
    }

    const mhopath = fs.readFileSync('mho.path');
    const mhopathstr = mhopath.toString().replace(/(\r\n|\n|\r)/gm, '');

    // get iipsdownloade/raw folder
    let iipsdownloadraw = path.join(mhopathstr, 'Bin/Client/IIPS/iipsdownload/raw');
    if (fs.existsSync('ifscontent.path')) {
      const ifscontent = fs.readFileSync('ifscontent.path');
      iipsdownloadraw = ifscontent.toString().replace(/(\r\n|\n|\r)/gm, '');
    }

    if (!fs.existsSync(iipsdownloadraw)) {
      console.log('iipsdownload raw folder is missing');
      return false;
    }

    this.getTSVReferenceContent().then((tsvjson) => {
      const tsvs = tsvjson.filter((tsv) => {
        return (!('disabled' in tsv));
      }).map(tsv => `${tsv.name}.dat`);

      console.log(`Copying ${tsvs.length} files to ${targetDir}`);

      this.loop(tsvs, (file, next) => {
        const sourceFile = path.join(iipsdownloadraw, `common/staticdata/${file}`);
        const targetFile = path.join(targetDir, file);
        if (!fs.existsSync(sourceFile)) {
          console.log(`${file} is missing please check`);
        } else {
          console.log('Copying', file, '...');
          this.copyFile(sourceFile, targetFile, next);
        }
      });
    }).catch(errors => console.log('Error when reading tsv file', errors));
  }

  /**
   * Recopy ui files from a src dir
   * referencing from ui.json
   */
  recopyuifromjson() {
    const targetDir = process.argv[3];

    if (targetDir === undefined) {
      console.log("TargetDir is missing");
      return false;
    }

    // read mho.path files contans where mho is installed
    if (!fs.existsSync('mho.path')) {
      console.log('mho.path is missing');
      return false;
    }

    // make sure target dir is exist
    if (!fs.existsSync(targetDir)) {
      fs.mkdirSync(targetDir);
    }

    const mhopath = fs.readFileSync('mho.path');
    const mhopathstr = mhopath.toString().replace(/(\r\n|\n|\r)/gm, '');

    // get uifolder
    let iipsdownloadraw = path.join(mhopathstr, 'Bin/Client/IIPS/iipsdownload/raw');
    if (fs.existsSync('ifscontent.path')) {
      const ifscontent = fs.readFileSync('ifscontent.path');
      iipsdownloadraw = ifscontent.toString().replace(/(\r\n|\n|\r)/gm, '');
    }

    if (!fs.existsSync(iipsdownloadraw)) {
      console.log('iipsdownload raw folder is missing');
      return false;
    }

    this.getUIReferenceContent().then((uijson) => {
      console.log(`Copying ${uijson.length} swf files to ${targetDir}`);

      this.loop(uijson, (ui, next) => {
        let file = `${ui.name}.swf`;
        let sourceFile = path.join(iipsdownloadraw, `libs/ui/widgets/${file}`);
        let targetFile = path.join(targetDir, file);

        // copy custom files to custom dir
        if ('custom' in ui && ui.custom) {
          let customDir = path.join(targetDir, 'custom');
          if (!fs.existsSync(customDir)) {
            fs.mkdirSync(customDir);
          }

          targetFile = path.join(customDir, file);
        }

        // if outside widgets folder copy it outside the sourcefile
        if ('outside' in ui && ui.outside) {
          sourceFile = path.join(iipsdownloadraw, `libs/ui/${file}`);
        }

        if (!fs.existsSync(sourceFile)) {
          console.log(`${file} is missing please check`);
        } else {
          console.log('Copying', file, 'to', targetFile);
          this.copyFile(sourceFile, targetFile, next);
        }
      });
    }).catch(errors => console.log('Error when reading ui file', errors));
  }

  /**
   * Copy a single file to directory
   * node mho --copyfile [source file] [target file]
   */
  copyfile() {
    const sourceFile = process.argv[3];
    const targetFile = process.argv[4];

    if (sourceFile === undefined) {
      this.die('Source file path is missing');
    }

    if (targetFile === undefined) {
      this.die('Target file path is missing');
    }

    this.copyFile(sourceFile, targetFile, () => {
      console.log('file copied to', targetFile);
    });
  }

  /**
   * Copy files from source to target directory
   * node mho --copyfiles [source dir] [target dir]
   */
  copyfiles() {
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is missing');
    }

    if (targetDir === undefined) {
      this.die('Target directory is missing');
    }

    // create target dir if not exist
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    const files = fs.readdirSync(sourceDir);
    console.log(`Copying ${files.length} files`);
    this.loop(files, (file, next) => {
      const sourceFile = path.join(sourceDir, file);
      const targetFile = path.join(targetDir, file);
      console.log('Copying', file, '...');
      this.copyFile(sourceFile, targetFile, next);
    });
  }

  /**
   * Rename a single file to directory
   * node mho --renamefile [source file] [target file]
   */
  renamefile() {
    const sourceFile = process.argv[3];
    const targetFile = process.argv[4];

    if (sourceFile === undefined) {
      this.die('Source file path is missing');
    }

    if (targetFile === undefined) {
      this.die('Target file path is missing');
    }

    this.renameFile(sourceFile, targetFile, () => {
      console.log('file renamed to', targetFile);
    });
  }

  /**
   * Rename files from source to target directory
   * node mho --renamefiles [source dir] [target dir]
   */
  renamefiles() {
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is missing');
    }

    if (targetDir === undefined) {
      this.die('Target directory is missing');
    }

    // create target dir if not exist
    if (!fs.existsSync(targetDir)){
      fs.mkdirSync(targetDir);
    }

    const files = fs.readdirSync(sourceDir);
    console.log(`Renaming ${files.length} files`);
    this.loop(files, (file, next) => {
      const sourceFile = path.join(sourceDir, file);
      const tofile = path.basename(file, path.extname(file));
      const targetFile = path.join(targetDir, tofile);
      console.log('Renaming', file, 'to', tofile);
      this.renameFile(sourceFile, targetFile, next);
    });
  }

  /**
   * Empty a directory
   */
  emptydir() {
    const sourceDir = process.argv[3];

    if (!fs.existsSync(sourceDir)) {
      this.die(`No such thing as ${sourceDir} directory`);
    }

    const rmspawn = spawn('rm', ['-rf', sourceDir]);
    rmspawn.stderr.on('data', (data) => {
      console.log(`Error removing: ${sourceDir}`);
    });

    rmspawn.on('close', (code) => {
      console.log(`${sourceDir} has been emptied`);

      // create back the removed parent folder
      fs.mkdirSync(sourceDir);
    });

    rmspawn.on('error', (err) => {
      console.log(`Error removing: ${sourceDir}`, err);
    });
  }
}

try {
  let optionArg = process.argv[2];
  if (optionArg && !!~optionArg.indexOf('--')) {
    let method = optionArg.replace('--', '');

    var mho = new MHO();
    if (mho.isMethodExist(method)) {
      mho.callMethod(method);
    } else {
      console.log("Invalid option: " + optionArg);
      console.log("Usage: node mho [options]");
    }
  } else {
    console.log("Usage: node mho [options]");
  }
} catch (err) {
  console.log("Error: " + err);
}


