import fs from 'fs';
import path from 'path';
import chalk from 'chalk';
import Spreadsheet from '../Spreadsheet';
import Common from '../Common';

class Download extends Common {

  /**
   * Upload all csv to gsheets
   * node mho --uploadallcsv [source dir] [gfolder_id]
   * node mho --uploadallcsv staticdata_csv_merge [id]
   */
  uploadallcsv() {

    const sourceDir = process.argv[3];
    const gfolder_id = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source dir is missing');
    }

    // set fodler ID to default folder
    let folderID = '0B2-sIhD0bUJdWDh6MmVsZmJiQjg';

    // set folderID if present
    if (gfolder_id !== undefined) {
      folderID = gfolder_id;
    }

    // read all files
    console.time('uploadallcsv');
    const files = fs.readdirSync(sourceDir);
    let filesToUpload = [];
    this.loop(files, (file, next) => {
      // only read csv
      if (path.extname(file) === '.csv') {
        filesToUpload.push(path.join(sourceDir, file));
      }

      next();
    }, () => {
      console.log('Uploading', filesToUpload.length, 'files');
      this._uploadcsv(filesToUpload, folderID).then(() => {
        console.timeEnd('uploadallcsv');
      }).catch((error) => console.log('uploadallcsv error', error));
    });
  }

  /**
   * Update all csv to gsheets
   * if the source dir has files that are not present
   * from the gfolder, it will automatically be upload
   * node mho --updateallcsv [source dir] [gfolderid]
   * node mho --uploadallcsv staticdata_csv_merge [id]
   */
  updateallcsv() {

    const sourceDir = process.argv[3];
    const gfolder_id = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source dir is missing');
    }

    // set fodler ID to default folder
    let folderID = '0B2-sIhD0bUJdWDh6MmVsZmJiQjg';

    // set folderID if present
    if (gfolder_id !== undefined) {
      folderID = gfolder_id;
    }

    // read all files
    console.time('updateallcsv');
    const files = fs.readdirSync(sourceDir);
    let filesToUpdate = [];
    this.loop(files, (file, next) => {
      // only read csv
      if (path.extname(file) === '.csv') {
        filesToUpdate.push(path.join(sourceDir, file));
      }

      next();
    }, () => {
      console.log('Updating', filesToUpdate.length, 'files');
      this._updatecsv(filesToUpdate, folderID).then(() => {
        console.timeEnd('updateallcsv');
      }).catch((error) => console.log('updateallcsv error', error));
    });
  }

  /**
   * download all csv files from google spreadsheets / gdrive folder to a target folder
   * node mho --downloadcsvfolder [target folder] [gfolderid]
   */
  downloadcsvfolder() {

    const targetFolder = process.argv[3];
    const folderID = process.argv[4];

    if (targetFolder === undefined) {
      this.die('Target dir is missing');
    }

    if (folderID === undefined) {
      this.die('Folder ID is missing');
    }

    const spreadsheet = new Spreadsheet();
    spreadsheet.downloadFolder(folderID).then((files) => {
      console.log('Downloading', files.length, 'files');

      // create target dir if not exist
      if (!fs.existsSync(targetFolder)){
        fs.mkdirSync(targetFolder);
      }

      files = files
        .filter(file => file.name !== '1Completion') // remove 1Completion file
        .filter(file => file.mimeType !== 'application/vnd.google-apps.folder'); // exclude folder ones

      let count = 0;
      this.loop(files, (file, next) => {
        const filename = file.name;
        const filepath = path.join(targetFolder, `${filename}.csv`);
        const fileId = file.id;
        const mimeType = file.mimeType;
        this.downloadcsv(filepath, fileId, filename, () => {
          count++;
          setTimeout(next, 100);
        });
      }, () => {
        console.log(`All ${count} csv files has been downloaded`)
      }, (err) => {
        console.log('Error when downloading files', err);
      });
    }).catch(errors => console.log(errors));
  }

  _uploadcsv(files = false, folderID = false) {
    return new Promise((resolve, reject) => {
      console.time('uploadcsv');
      console.log('Spreadsheet upload mode');

      if (!files) {
        console.log('Files to upload is missing');
        process.exit(1);
      }

      if (!folderID) {
        console.log('Folder ID is missing');
        process.exit(1);
      }

      const spreadsheet = new Spreadsheet();
      this.loop(files, (file, next) => {
        const filename = path.basename(file, path.extname(file));
        spreadsheet.upload(`${file}`, filename, folderID).then(f => {
          console.log(filename, 'upload:', f.id);
          next();
        }).catch(reject);
      }, () => {
        console.timeEnd('uploadcsv');
        resolve();
      });
    });
  }

  uploadcsvsingle() {
    const filepath = process.argv[3];
    const folderID = process.argv[4];

    if (filepath === undefined) {
      this.die('File to upload is missing');
    }

    if (folderID === undefined) {
      this.die('Folder ID is missing');
    }

    let filename = path.basename(filepath, path.extname(filepath));
    const spreadsheet = new Spreadsheet();
    spreadsheet.upload(filepath, filename, folderID).then(f => {
      console.log(filename, 'upload:', f.id);
    }).catch((err) => {
      console.log('Upload error', err);
    });
  }

  _updatecsv(sourcefiles = false, folderID) {
    return new Promise((resolve, reject) => {
      console.log('Spreadsheet update mode');

      if (!sourcefiles) {
        console.log('Files to update is missing');
        process.exit(1);
      }

      if (!folderID) {
        console.log('Folder ID is missing');
        process.exit(1);
      }


      // lets download the folder to get the file ID
      // we are updating
      const spreadsheet = new Spreadsheet();
      let filesUpdatedCount = 0;
      let filesUploadedCount = 0;
      spreadsheet.downloadFolder(folderID).then((files) => {
        console.log('Downloaded', files.length, 'files from folder', folderID);

        console.time('updatecsv');
        this.loop(sourcefiles, (file, next) => {
          const filename = path.basename(file, path.extname(file));

          // find the meta data of the source file
          // from the downloaded files
          const metadata = files.find(file => file.name === filename);
          if (metadata) {
            spreadsheet.update(`${file}`, metadata, folderID).then(file => {
              console.log(filename, 'update:', file.id);
              filesUpdatedCount++;
              next();
            }).catch(reject);
          } else {
            spreadsheet.upload(`${file}`, filename, folderID).then(file => {
              console.log(filename, 'upload:', file.id);
              filesUploadedCount++;
              next();
            }).catch(reject);
          }
        }, () => {
          console.log('Files updated:', filesUpdatedCount);
          console.log('Files uploaded:', filesUploadedCount);
          console.timeEnd('updatecsv');
          resolve();
        }, () => {
          reject('Folder is empty no files to update');
        });
      });
    });
  }

  updatecsvsingle() {
    const filepath = process.argv[3];
    const fileid = process.argv[4];
    const folderID = process.argv[5];

    if (filepath === undefined) {
      this.die('File to update is missing');
    }

    if (fileid === undefined) {
      this.die('File id is missing');
    }

    if (folderID === undefined) {
      this.die('Folder ID is missing');
    }

    let filename = path.basename(filepath, path.extname(filepath));
    const spreadsheet = new Spreadsheet();
    spreadsheet.update(filepath, {id: fileid, name: filename}, folderID).then(f => {
      console.log(filename, 'update:', f.id);
    }).catch((err) => {
      console.log('Update error', err);
      console.log('\u0007');
    });
  }

  downloadcsv(filepath, fileId, filename, cb = false) {
    // console.log('Spreadsheet mode', fileId, filename);
    const spreadsheet = new Spreadsheet();
    spreadsheet.read(fileId, filename).then(rows => {
      if (rows.length > 0) {
        let table = [];
        rows.forEach(line => {
          table.push(line.join(this.getTabDelimiter()));
        });

        const file = table.join(this.getNewLineDelimiter());
        fs.writeFile(filepath, file, (err) => {
          if (err) throw err;
            console.log(chalk.green('OK'), `${filename} downloaded`, chalk.yellow(`${this.humanFilesize(file.length)}`));
          cb && cb();
        });
      }
    }).catch((error) => {
      console.log(chalk.red('ERROR'), error);
      console.log('\u0007');
    });
  }

  downloadtestcsv() {
    const spreadsheet = new Spreadsheet();
    spreadsheet.downloadDocumentTest();
  }
}

export default Download;
