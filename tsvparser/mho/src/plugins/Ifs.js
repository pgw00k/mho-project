import fs from 'fs';
import path from 'path';
import jsonfile from 'jsonfile';
import { exec, spawn } from 'child_process';

class Ifs {

  version() {
    let { version } = this._getpatchInfo();
    console.log(version);
  }

  /**
   * Create a patch
   * everytime this is exeuted increase minor version
   * if created for public increase the major version and reset minor version
   * make sure `ifs/patch` folders contains the files
   * node mho --createpatch
   */
  createpatch() {

    let forpublic = process.argv[3];

    // read the patch version
    let info = this._getpatchInfo();
    let { version } = info;
    let [ release, major, minor ] = version.split('.');

    // for public release
    if (forpublic !== undefined) {
      // increase major
      major++;

      // reset minor
      minor = 0;
    } else {
      minor++;
    }

    // update
    info.version = [ release, major, minor ].join('.');

    console.time('createpatch');
    exec('mho.exe --create', {
      cwd: 'ifs/'
    }, (err, stdout, stderror) => {
      if (err) {
        console.error(err);
      } else {
        console.log(stdout);

        if (stderror) {
          console.log(stderror);
        }

        // save new version
        jsonfile.writeFile('package.json', info, {spaces: 2}, (err) => {
          if (err) this.die(err);
          console.log(`Patch ${info.version} has been created`);
          console.timeEnd('createpatch');
        });

        // create patch.version
        let patchversion = 'patchversion.txt';
        // if (fs.existsSync(patchversion)) {
        //   fs.unlink(patchversion);
        // }

        fs.writeFile(patchversion, info.version, (err) => {
          if (err) throw err;
        });
      }
    });
  }

  /**
   * Extract the files from ifs
   * make sure the ifs file is inside the `ifs/list` directory
   * node mho --extractifs [IFS name] [optional target dir]
   */
  extractifs() {
    console.time('extractifs');

    const ifsFilename = process.argv[3];
    if (ifsFilename === undefined) {
      console.log(`Missing IFS file`);
      process.exit(1);
    }

    const cwd = 'ifs';
    const ifsFilePath = path.join('list', ifsFilename);
    const patchListFolder = path.join(cwd, ifsFilePath);
    const targetFolder = (process.argv[4] !== undefined) ? process.argv[4] : 'extracted';

    // check target dir
    if (!fs.existsSync(patchListFolder)){
      console.log(`Missing IFS file on ${patchListFolder}`);
      process.exit(1);
    }

    // console.log(patchListFolder);
    // console.log(ifsFilePath);
    // console.log(targetFolder);
    console.log(`Extracting files from ${ifsFilePath}`);

    const child = spawn('mho.exe', ['--extract', ifsFilePath, targetFolder], {
      cwd
    });

    let hasStaticdata = false;
    let fileCount = 0;
    let staticFilecount = 0;
    child.stdout.on('data', function (data) {
      // if theres a static data
      if (!!~data.indexOf(`\\staticdata\\`)) {
        hasStaticdata = true;
        staticFilecount++;
        // console.log(`${data}`);
      }

      fileCount++;
    });

    child.stderr.on('data', function (data) {
      console.log('stderr: ' + data);
    });

    child.on('close', function (code) {
      console.log("Extraction completed with", fileCount, 'files in total.');
      console.log('Staticdata files:', staticFilecount);
    });

    child.on('error', (err) => {
      console.log('Failed to start child process.');
    });
  }

  _getpatchInfo() {
    return jsonfile.readFileSync('package.json');
  }
}

export default Ifs;
