import fs from 'fs';
import path from 'path';
import Common from '../Common';
import TsvMerger from '../TsvMerger';
import TsvExtractor from '../TsvExtractor';
import TsvImporter from '../TsvImporter';

import EncDec from './EncDec';

class Tsv extends Common {

  /**
   * extract the text content of a all the tsv to the target directory
   * the files must should already decrypted
   * node mho --tsvextractall [source dir] [target dir]
   */
  tsvextractall(cb = false) {
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is missing');
    }

    if (targetDir === undefined) {
      this.die('Target directory is missing');
    }

    // get the whole ref content
    this.getTSVReferenceContent().then((tsvjson) => {
      const tsvExtractor = new TsvExtractor();

      // read all files
      // console.time('tsvextract');
      const files = fs.readdirSync(sourceDir);
      this.loop(files, (file, next) => {
        // find tsv ref of the current tsv on the handler
        const currTsvname = file.replace('.dat', '');
        const tsvref = tsvjson.find(tsv => {
          return tsv.name === currTsvname && !('disabled' in tsv)
        });

        const sourceFile = `${sourceDir}/${file}`;

        // some files aren't included on the tsv
        // ignore them
        if (tsvref) {
          tsvExtractor.setTsvRef(tsvref);
          tsvExtractor.extract(sourceFile, targetDir, next);
        } else {
          next();
        }
      }, () => {
        cb && cb(targetDir);
      }, () => {
        this.die("Could not list the directory: ", sourceDir);
      });
    }).catch((err) => {
      this.die("Could not read the tsv json", err);
    });
  }

  /**
   * import a multiple text content to multiple tsvfiles
   * output file is a decrypted form of a tsv
   * node mho --tsvimportall [source dir] [target dir] [reference dir]
   * node mho --tsvimportall staticdata_csv_merge staticdata_dec_translated staticdata_dec
   */
  tsvimportall(cb = false) {

    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];
    const referenceDir = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is missing');
    }

    if (targetDir === undefined) {
      this.die('Target directory is missing');
    }

    if (referenceDir === undefined) {
      this.die('Reference directory is missing');
    }

    this.getTSVReferenceContent().then((tsvjson) => {

      // read all files
      // console.time('tsvimportall');

      const files = fs.readdirSync(sourceDir);
      this.loop(files, (file, next) => {
        // find tsv ref of the current tsv on the handler
        const currTsvname = file.split('.')[0];
        const tsvref = tsvjson.find(tsv => {
          return tsv.name === currTsvname && !('disabled' in tsv)
        });

        const sourceFile = `${sourceDir}/${file}`;

        // some files aren't included on the tsv
        // ignore them
        if (tsvref) {
          const tsvImporter = new TsvImporter();
          tsvImporter.setTsvRef(tsvref);
          tsvImporter.import(sourceFile, targetDir, referenceDir, next);
        } else {
          next();
        }
      }, () => {
        cb && cb(targetDir);
      }, () => {
        this.die("Could not list the directory: ", sourceDir);
      });
    });
  }

  /**
   * like `tsvimportall` but the final step is to encrypt everything
   * node mho --tsvimportencall [source csv] [target dat] [target enc]
   * sample:
   * node mho --tsvimportencall staticdata_csv_merge staticdata_dec_translated staticdata_enc_translated
   */
  tsvimportencall() {
    const encdec = new EncDec();
    this.tsvimportall(sourceDir => {
      encdec._cryptall('encall', 'encrypt', [
        sourceDir, process.argv[5]
      ]);
    });
  }

  /**
   * Usage
   * node mho --tsvextractdecall [source enc] [target dec] [target csv]
   * sample:
   * node mho --tsvextractdecall staticdata_enc staticdata_dec staticdata_csv
   */
  tsvextractdecall() {

  }

  /**
   * Merge old and new tsv - via csv
   * node mho --tsvmerge [tsvname]
   */
  tsvmerge() {
    const tsvMerger = new TsvMerger();
    const tsvname = process.argv[3];
    console.log('Merging', tsvname);
    tsvMerger.merge(tsvname);
  }
}

export default Tsv;
