import fs from 'fs';
import path from 'path';
import Common from '../Common';
import UIHandler from '../UIHandler';
import jsonfile from 'jsonfile';

class UiPlugin extends Common {

  /**
   * Test and check ffdec library
   */
  testffdec() {
    const uiHandler = new UIHandler();
    uiHandler.testffdec();
  }

  exportswfabc() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];

    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.exportswfabc(source, target);
  }

  assembleabc() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];

    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.assembleabc(source, target);
  }

  replaceswfabc() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];
    let reference = process.argv[5];

    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    if (reference === undefined) {
      this.die('Reference directory is required');
    }

    uiHandler.replaceswfabc(source, target, reference);
  }

  translateabc() {
    const uiHandler = new UIHandler();
    let target = process.argv[3];
    let reference = process.argv[4];

    if (target === undefined) {
      this.die('Target directory is required');
    }

    if (reference === undefined) {
      this.die('Translation directory is required');
    }

    uiHandler.translateabc(target, reference);
  }

  exportswftextcode() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];

    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.exportswftextcode(source, target);
  }

  textcode2swf() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];
    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.textcode2swf(source, target);
  }

  swf2imagecode() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];
    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.swf2imagecode(source, target);
  }

  imagecode2swf() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let target = process.argv[4];
    if (source === undefined) {
      this.die('Source directory is required');
    }

    if (target === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.imagecode2swf(source, target);
  }

  translatetextcode() {
    const uiHandler = new UIHandler();
    let source = process.argv[3];
    let sourcetranslation = process.argv[4];

    if (source === undefined) {
      this.die('Source file is required');
    }

    if (sourcetranslation === undefined) {
      this.die('Source translation is required');
    }

    uiHandler.translatetextcode(source, sourcetranslation);
  }

  /**
   * Extract the xml of a swf
   * make sure every swf to extract is inside
   * `uiswf` folder from root or from specified source
   * this extract the xml files on the same folder
   * node mho --swf2xml [target dir]
   */
  swf2xml() {
    const uiHandler = new UIHandler();
    let sourceDir = process.argv[3];
    let targetDir = process.argv[4];
    let allfiles = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    allfiles = (allfiles === undefined) ? false : true;

    uiHandler.swf2xml(sourceDir, targetDir, allfiles);
  }

  /**
   * Import the xml to swf
   * node mho --xml2swf [sourcedir] [target dir]
   */
  xml2swf() {
    const uiHandler = new UIHandler();
    let sourceDir = process.argv[3];
    let targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.xml2swf(sourceDir, targetDir);
  }

  /**
   * Same as xml2swf but for single files
   */
  xml2swfsingle() {
    const uiHandler = new UIHandler();
    let sourceFile = process.argv[3];
    let targetFile = process.argv[4];

    if (sourceFile === undefined) {
      this.die('Source file is required');
    }

    if (targetFile === undefined) {
      this.die('Target file is required');
    }

    uiHandler.xml2swfsingle(sourceFile, targetFile);
  }

  /**
   * extract the csv version of swfxml
   * `sourceDir` where to get the swfxml from
   * `targetDir` where to save the extracted csv
   * `mhui path` mhui file - the base file of every swf(if this is present
   * every text that is part of the file will not be included
   * on the current parsed translation)
   * node mho --swfxml2csv [swfxml sourceDir] [csv targetDir]
   */
  swfxml2csv() {
    const uiHandler = new UIHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];
    const mhuifile = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.swfxml2csv(sourceDir, targetDir, mhuifile);
  }

  /**
   * Same as swfxml2csv but for single files
   */
  swfxml2csvsingle() {
    const uiHandler = new UIHandler();
    const sourceFile = process.argv[3];
    const targetFile = process.argv[4];
    const mhuifile = process.argv[5];

    if (sourceFile === undefined) {
      this.die('Source file is required');
    }

    if (targetFile === undefined) {
      this.die('Target file is required');
    }

    uiHandler.swfxml2csvsingle(sourceFile, targetFile, mhuifile);
  }

  /**
   * NOTE: for this function to work properly
   * the xml2js library should be modified to wrap the
   * rdf tag inside CDATA
   * if (!!~child.indexOf('<rdf:RDF')) {
   *   element = element.raw(wrapCDATA(child));
   * }
   * import the csv to swfxml
   * `sourceDir` where to get the csv from
   * `targetDir` where to save the modified xml
   * `referenceDir` where to get the original xml files
   * node mho --csv2swfxml [csv sourceDir] [swfxml targetDir] [reference Dir]
   * Sample:
   * node mho --csv2swfxml uiswf uiswf_ready uiswf
   */
  csv2swfxml() {
    const uiHandler = new UIHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];
    const referenceDir = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    if (referenceDir === undefined) {
      this.die('Reference directory is required');
    }

    uiHandler.csv2swfxml(sourceDir, targetDir, referenceDir);
  }

  /**
   * Same as csv2swfxml but for single file
   */
  csv2swfxmlsingle() {
    const uiHandler = new UIHandler();
    const sourceFile = process.argv[3];
    const targetFile = process.argv[4];
    const referenceFile = process.argv[5];

    if (sourceFile === undefined) {
      this.die('Source file is required');
    }

    if (targetFile === undefined) {
      this.die('Target file is required');
    }

    if (referenceFile === undefined) {
      this.die('Reference file is required');
    }

    uiHandler.csv2swfxmlsingle(sourceFile, targetFile, referenceFile);
  }

  /**
   * Extract the csv of the ui xml
   * these xml are the files under the `xml` folder
   * `sourceDir` where to get the uixml from
   * `targetDir` where to save the extracted csv
   * node mho --uixml2csv [uixml sourceDir] [csv targetDir]
   */
  uixml2csv() {
    const uiHandler = new UIHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    uiHandler.uixml2csv(sourceDir, targetDir);
  }

  /**
   * import the csv to uixml
   * `sourceDir` where to get the csv from
   * `targetDir` where to save the modified xml
   * `referenceDir` where to get the original xml files
   * node mho --csv2uixml [csv sourceDir] [uixml targetDir] [reference Dir]
   * Sample:
   * node mho --csv2uixml uixml uixml_ready uixml
   */
  csv2uixml() {
    const uiHandler = new UIHandler();
    const sourceDir = process.argv[3];
    const targetDir = process.argv[4];
    const referenceDir = process.argv[5];

    if (sourceDir === undefined) {
      this.die('Source directory is required');
    }

    if (targetDir === undefined) {
      this.die('Target directory is required');
    }

    if (referenceDir === undefined) {
      this.die('Reference directory is required');
    }

    uiHandler.csv2uixml(sourceDir, targetDir, referenceDir);
  }

  createswfjson() {
    const sourceDir = process.argv[3];
    const files = fs.readdirSync(sourceDir)
      .filter(file => fs.statSync(path.join(sourceDir, file)).isFile())
      .map(file => {
        return {
          name: path.basename(file, path.extname(file))
        }
      });

    jsonfile.writeFile('ui.json', files, {spaces: 2}, function(err) {
      err && console.error(err);

      console.log('Done');
    });
  }
}

export default UiPlugin;
