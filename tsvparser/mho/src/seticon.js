var path = require('path');
var fs = require('fs');
var spawn = require('child_process').spawn;

var infile = process.argv[2];
var outfile = process.argv[3];
var iconfile = process.argv[4];

if (!fs.existsSync(infile)) {
  console.log('Infile', infile, 'is missing');
  process.exit(1);
}

if (!fs.existsSync(iconfile)) {
  console.log('Icon', iconfile, 'is missing');
  process.exit(1);
}

infile = path.resolve(infile);
outfile = path.resolve(outfile);
iconfile = path.resolve(iconfile);
var exe = path.join('D:\\Program Files (x86)\\Resource Hacker', 'ResourceHacker.exe');
if (!fs.existsSync(exe)) {
  console.log('Resource Hacker is missing on this path', exe);
  process.exit(1);
}

var command = "-open "+infile+" -save "+outfile+" -action addskip -res "+ iconfile +" -mask ICONGROUP,MAINICON,";
console.log(command);

var child = spawn(exe, command.split(' '));

child.stdout.on('data', (data) => {
  console.log(data.toString());
});

child.stderr.on('data', (data) => {
  console.log("Seticon error", data);
});

child.on('close', function (code) {
  console.log("Icon has been set to", outfile);
});

child.on('error', (err) => {
  console.log("Failed to set icon", err);
});
