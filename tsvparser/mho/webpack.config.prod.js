var path = require('path');
var webpack = require('webpack');
var fs = require('fs');
var nodeExternals = require('webpack-node-externals');
var PrepackWebpackPlugin  = require('prepack-webpack-plugin').default;

module.exports = {
  entry: path.join(__dirname, 'src/index.js'),
  target: 'node',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'index.js'
  },
  externals: [nodeExternals()],
  plugins: [
    new PrepackWebpackPlugin({})
  ],
  // resolve: {
  //   extensions: ['', '.js', '.jsx'],
  //   modulesDirectories: ['node_modules', 'src']
  // },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
      { test: /\.json$/, loader: 'json-loader' }
    ]
  },
  devtool: 'sourcemap'
};