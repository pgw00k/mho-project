Steps.
1. Extract tsv from IFS - extractifs and ./mho-patch --recopyenc "G:\PC Games\Tencent\Monster Hunter Online\Bin\Client\IIPS\iipsdownload\new\common\staticdata" staticdata_enc
2. Download sheets - ./mho-patch --downloadcsvfolder downloaded_csv 0B2-sIhD0bUJdWDh6MmVsZmJiQjg
3. decrypt tsv and extract them to a folder - ./mho-patch --decall staticdata_enc staticdata_dec
4. extract csv from tsv - ./mho-patch --tsvextractall staticdata_dec staticdata_csv
5. Merge downloaded sheets with the extracted csv files - ./mho-patch --csvmerge staticdata_csv downloaded_csv staticdata_csv_merge
6. import csv to tsv - ./mho-patch --tsvimportall staticdata_csv_merge staticdata_dec_translated staticdata_dec
7. encrypt the tsv files - ./mho-patch --encall staticdata_dec_translated staticdata_enc_translated
7. copy files and create patch - ./usepatch
8. Upload the merged csvs - ./mho-patch --updateallcsv staticdata_csv_merge 0B2-sIhD0bUJdWDh6MmVsZmJiQjg

./mho-patch --uploadcsvsingle "staticdata_csv_merge/giftbagdata.csv" 0B2-sIhD0bUJdWDh6MmVsZmJiQjg
./mho-patch --updatecsvsingle "staticdata_csv_merge/title.csv" 1VbPxKxSIjh2p_x-F9Gk4UE99G3d55TZbhbJKxxZH1NQ 0B2-sIhD0bUJdWDh6MmVsZmJiQjg

Short version
./mho-patch --downloadcsvfolder downloaded_csv 0B2-sIhD0bUJdWDh6MmVsZmJiQjg

./mho-patch --recopyencfromtsv staticdata_enc && ./mho-patch --decall staticdata_enc staticdata_dec && ./mho-patch --tsvextractall staticdata_dec staticdata_csv && ./mho-patch --csvmerge staticdata_csv downloaded_csv staticdata_csv_merge && ./mho-patch --tsvimportall staticdata_csv_merge staticdata_dec_translated staticdata_dec && ./mho-patch --encall staticdata_dec_translated staticdata_enc_translated

./mho-patch --csvmerge staticdata_csv downloaded_csv staticdata_csv_merge && ./mho-patch --tsvimportall staticdata_csv_merge staticdata_dec_translated staticdata_dec && ./mho-patch --encall staticdata_dec_translated staticdata_enc_translated

for UI
1. download ui - ./mho-patch --downloadcsvfolder "ui/downloaded_ui" 0B2-sIhD0bUJdNnFDbFA3QktaRDg
2. extract xml from swf - ./mho-patch --swf2xml "ui/swf" "ui/swfxml"
3. extract csv from xml - ./mho-patch --swfxml2csv "ui/swfxml" "ui/csv"
4. merge downloaded csv to extracted csv - ./mho-patch --csvmerge "ui/csv" "ui/downloaded_ui" "ui/csv_merge"
5. import csv to xml - ./mho-patch --csv2swfxml "ui/csv_merge" "ui/swfxml_ready" "ui/swfxml"
6. import xml to swf - ./mho-patch --xml2swf "ui/swfxml_ready" "ui/swf_ready"
7. update csv to gsheet - ./mho-patch --updateallcsv "ui/csv_merge" 0B2-sIhD0bUJdNnFDbFA3QktaRDg

./mho-patch --recopyencfromjson staticdata_enc
./mho-patch --recopyuifromjson ui/swf
./mho-patch --charlimits downloaded_csv/weaponrulesdata.csv

./mho-patch --csvmerge "ui/csv" "ui/downloaded_ui" "ui/csv_merge" && ./mho-patch --csv2swfxml "ui/csv_merge" "ui/swfxml_ready" "ui/swfxml" && ./mho-patch --xml2swf "ui/swfxml_ready" "ui/swf_ready"

extract single swf file

./mho-patch --swfxml2csvsingle "ui/swfxml/meicityview.xml" "ui/csv/meicityview.ui.csv" && ./mho-patch --copyfile "ui/csv/meicityview.ui.csv" "ui/downloaded_ui/meicityview.ui.csv" && ./mho-patch --csvmerge "ui/csv" "ui/downloaded_ui" "ui/csv_merge" && ./mho-patch --uploadcsvsingle "ui/csv_merge/meicityview.ui.csv" 0B2-sIhD0bUJdNnFDbFA3QktaRDg

updating single swf file

./mho-patch --csvmerge "ui/csv" "ui/downloaded_ui" "ui/csv_merge" && ./mho-patch --csv2swfxmlsingle "ui/csv_merge/meicityview.ui.csv" "ui/swfxml_ready/meicityview.xml" "ui/swfxml/meicityview.xml" && ./mho-patch --xml2swfsingle "ui/swfxml_ready/meicityview.xml" "ui/swf_ready/meicityview.swf"

./mho-patch --uploadcsvsingle "ui/csv_merge/farmlargefacilitiesmainview.ui.csv" 0B2-sIhD0bUJdNnFDbFA3QktaRDg
./mho-patch --updatecsvsingle "ui/csv_merge/meicityview.ui.csv" 1aZKktwWHVnmT6auIp0Wv_QzCBIjnIVwKShenXLIKI4Q 0B2-sIhD0bUJdNnFDbFA3QktaRDg

for abc/textcode

./mho-patch --csvmerge "ui/csv" "ui/downloaded_ui" "ui/csv_merge" && ./mho-patch --exportswfabc "ui/swf/custom" "ui/abc" && ./mho-patch --translateabc "ui/abc" "ui/csv_merge" && ./mho-patch --assembleabc "ui/abc" "ui/abc_ready" && ./mho-patch --replaceswfabc "ui/abc_ready" "ui/swf_ready" "ui/swf/custom" && ./mho-patch --exportswftextcode "ui/swf/custom" "ui/textcode" && ./mho-patch --translatetextcode "ui/textcode" "ui/csv_merge" && ./mho-patch --textcode2swf "ui/textcode" "ui/swf_ready"

for imagecode

./mho-patch --imagecode2swf "ui/imagecode" "ui/swf_ready"

for ui xml

./mho-patch --uixml2csv "ui/xml" "ui/csv"
./mho-patch --csv2uixml "ui/csv_merge" "ui/xml_ready" "ui/xml"
./mho-patch --uploadcsvsingle "ui/csv_merge/facialconfig.xml.csv" 0B2-sIhD0bUJdNnFDbFA3QktaRDg

Folder meanings

staticdat_enc - contains all the encrypted tsv/dat files from the game

staticdata_dec - contains all the decrypted files which usually comes from staticdata_enc

staticdata_csv - the extracted text from the tsv/dat files which usually comes from staticdata_dec

staticdata_csv_merge - (can be user generated) contains all the merged files of the downloaded csv(from downloaded_csv) over the extracted csv(from staticdata_csv)

downloaded_csv - (can be user generated) contains all the files downloaded from gsheet folder

staticdata_dec_translated - contains the translated tsv/dat files, usually comes from staticdata_csv or staticdata_csv_merge

staticdata_enc_translated - contains the translated and already encrypted tsv/dat files, usually comes from staticdata_translated first

staticdata_uixml - contains all the UI xml text

staticdata_uixml_ready - contains the translate UI xml text


